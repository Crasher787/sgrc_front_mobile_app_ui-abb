package com.example.isitoq_project;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.isitoq_project.servercontroller.HttpController;
import com.example.topsheetlib.TopSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

public class uzel_uzel extends AppCompatActivity {

    public static String NodeUid = "";
    public static String NodeName = "";
    public static String NodeDescription = "";
    public static String  NodeDateCreation = "";

    private int NumberPage = 0;
    private ImageButton Gotoback;
    private Button ShowThis;
    private ImageButton AddCom;
    public MaterialButton Uzel;
    private MaterialButton Log;

    TextView nodeName;
    TextView nodeUid;
    TextView nodeDescription;
    TextView nodeDateCreation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uzel_uzel);

        Gotoback = findViewById(R.id.GoTOiRP);
        Gotoback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main_page.NumPageEliment = 11;
                takeScreenshot();
                Intent intent = new Intent(uzel_uzel.this, Main_page.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_sidein, R.anim.left_slideout);
                finish();

            }
        });
        ShowThis = findViewById(R.id.ShowMore);
        ShowThis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenTopMenuLog(v);

            }
        });
        AddCom = findViewById(R.id.add_itemUzelLogs);
        AddCom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openBottomSheetDialogAddEventsFromUzel(v);


            }
        });

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setDataToPage();
    }

    public void OpenTopMenuLog(View view){
        View sheet = findViewById(R.id.top_sheetOpenUzel);
        TopSheetBehavior.from(sheet).setState(TopSheetBehavior.STATE_EXPANDED);
        Uzel = sheet.findViewById(R.id.button_uzel);
        Log = sheet.findViewById(R.id.button_logss);

        if(NumberPage == 0)
        {
            //Изменения цвета кнопки
            Uzel.setTextColor(Color.parseColor("#3F8AE0"));
            Uzel.setTextColor(Color.parseColor("#818C99"));
            Log.setTextColor(Color.parseColor("#818C99"));
            Log.setTextColor(Color.parseColor("#818C99"));
        }


        sheet.findViewById(R.id.button_uzel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("kok");
                NumberPage = 0;
                if(NumberPage == 0)
                {
                    //Изменения цвета кнопки
                    Uzel.setTextColor(Color.parseColor("#3F8AE0"));
                    Uzel.setIconTint(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
                    Log.setTextColor(Color.parseColor("#818C99"));
                    Log.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                }


            }
        });



        sheet.findViewById(R.id.button_logss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPage = 1;
                if(NumberPage == 1)
                {
                    Uzel.setTextColor(Color.parseColor("#818C99"));
                    Uzel.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Log.setTextColor(Color.parseColor("#3F8AE0"));
                    Log.setIconTint(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
                }


            }
        });


    }

    public void openBottomSheetDialogAddEventsFromUzel(View view){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_newevents);
        bottomSheetDialog.show();
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/pictures" + "/tmp_screen.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    public void backgroundWork(String uid) {
        Runnable runnable = new Runnable() {
            public void run() {
                HttpController httpController = new HttpController();
                try {
                    httpController.getNodeByUid(uid);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void setDataToPage() {
        nodeName = (TextView) findViewById(R.id.nodeName);
        nodeUid = (TextView) findViewById(R.id.nodeUid);
        nodeDescription = (TextView) findViewById(R.id.nodeDescription);
        nodeDateCreation = (TextView) findViewById(R.id.nodeDateCreation);

        nodeName.setText(NodeName);
        nodeUid.setText(NodeUid);
        nodeDescription.setText(NodeDescription);

        SimpleDateFormat formatBefore = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat formatAfter = new SimpleDateFormat("dd.MM.yyyy' в 'HH:mm:ss");


        try {
            Date date = formatBefore.parse(NodeDateCreation);
            String dateString = formatAfter.format(date);
            System.out.println(date);
            nodeDateCreation.setText(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}


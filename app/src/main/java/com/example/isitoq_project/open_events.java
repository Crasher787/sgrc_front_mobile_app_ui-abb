package com.example.isitoq_project;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.example.isitoq_project.servercontroller.HttpController;
import com.example.topsheetlib.TopSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class open_events extends AppCompatActivity  {

    //Это нужно для
//implements DatePickerDialog.OnDateSetListener

    public static JSONArray eventsArray = new JSONArray();
//    private LinearLayout mBottomSheet;
//    private BottomSheetBehavior mBottomSheetBehavior;
    private int NumberPage = 1;
    private String NamePage = "Мои события";
    private ImageButton OpenFilter;
    private TextInputLayout deptLay;
    private LinearLayout StatusLiner;
    private LinearLayout TegLiner;
    private Button ShowButtons;
    private AutoCompleteTextView cardSort;
    private AutoCompleteTextView cardDep;
    private AutoCompleteTextView cardStatus;
    private AutoCompleteTextView cardTeg;

    private AutoCompleteTextView cardF;
    private AutoCompleteTextView cardCritp;
    private AutoCompleteTextView cardFQDN;
    private AutoCompleteTextView cardTeg1;
    private AutoCompleteTextView cardDep1;
    private AutoCompleteTextView cardOrigin1;
    private AutoCompleteTextView cardDeadLine;
    private AutoCompleteTextView cardDM;
    private AutoCompleteTextView cardDic;

    private AutoCompleteTextView cardDangenMMen;
    private AutoCompleteTextView cardAddsis;
    private AutoCompleteTextView CardCow;
    private AutoCompleteTextView cardMone;
    private AutoCompleteTextView cardIp;
    private AutoCompleteTextView cardBedBoy;
    private AutoCompleteTextView cardFQDNUzel;
    private AutoCompleteTextView cardDateOpenTotot;
    private AutoCompleteTextView CardTimeOpenDo;
    private LinearLayout LinerFiltetop;

    private CheckBox ShowAllSet;
    public TextView OpenEvent;
    private TextView MyEvent;
    private TextView WatchEvent;
    private TextView ControlEvent;
    private TextView ExitEvent;
    private TextView MyQuryEvent;
    private TextView CloseQuryEvent;

    public TextView OpenEventText;
    private TextView MyEventText;
    private TextView WatchEventText;
    private TextView ControlEventText;
    private TextView ExitEventText;
    private TextView MyQuryEventText;
    private TextView CloseQuryEventText;
    private Button showTickets;

    private ImageButton GoIrp;
    private ImageButton addEvents;

    int mOriginalHeight = 0;
    boolean initialSizeObtained = false;
    boolean isShrink = false;

//    private AutoCompleteTextView cardOring;
//    private AutoCompleteTextView cardFIO;
//    private MultiAutoCompleteTextView cardRole;
//    private AutoCompleteTextView cardNameEvent;
//    private AutoCompleteTextView cardFQDN;
//    private AutoCompleteTextView cardDateOpenTo;
//    private AutoCompleteTextView cardDateOpenDo;
//    private AutoCompleteTextView cardDateCloseTo;
//    private AutoCompleteTextView cardDateCloseDo;
    Calendar dateAndTime = Calendar.getInstance();

    private int NumIf = 0;

    public static String[] data1 = {"Test"};
    public static String[] AllUser = {"Test"};
    public static String[] data2 = {"Test"};
//    public static String[] dataRole = {"Наблюдатель","Соиспольнитель","Ответственный","Контролирующий"};
    public static String[] dataSort = {"Сначала срочные","По порядку ID","По названиям(А-Я) ","По времени"};
    public static String[] datadep = {"Test"};
    public static String[] dataStatus = {"Открыт","В работе","Возвращён в работу","Выполнен","Закрыт","Отклонён"};
    public static String[] dataTag = {"Test"};
//    TextInputLayout sort;
//    AutoCompleteTextView cardochka;
//    ArrayList<String> arrayList_sort;
//    ArrayAdapter<String> arrayAdapter_sort;
    private ImageButton ExitButton;

//    String[] data = {"Открытые события", "Мои события", "Наблюдаю", "Контролирую", "Завершенные","Мои запросы на отклонение"};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_open_events);
        showTickets = findViewById(R.id.ShowTickets);
        showTickets.setText(NamePage);


        //Выдвигающееся меню снизу
//        mBottomSheet = findViewById(R.id.bottom_sheelt1);
//        mBottomSheetBehavior = BottomSheetBehavior.from(mBottomSheet);
//        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        ShowButtons = findViewById(R.id.ShowTickets);
        ShowButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTopSheetDialog(v);
            }
        });

        addEvents = findViewById(R.id.add_itemEVENTS);
        addEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheetDialogAddEvents(v);
            }
        });



        GoIrp = findViewById(R.id.MenuIrpGo);
        GoIrp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Main_page.NumPageEliment = 3;
                    takeScreenshot();
                    Intent intent = new Intent(open_events.this, Main_page.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_sidein, R.anim.left_slideout);
                    finish();
                } catch (Exception e) {

                }
            }
        });

        OpenFilter = findViewById(R.id.ButtonOpenFilterEvents);
        OpenFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheetDialog(v);
            }

        });


        // чтобы не было видно верхней фиолетовой фигни
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        changeEvents();


        // адаптер
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                R.layout.spinner3_elements, R.id.spinner_events, data);

//        Spinner spinner3 = (Spinner) findViewById(R.id.spinner3);
//        spinner3.setAdapter(adapter);
//        // заголовок
//        spinner3.setPrompt("Title");
//        // выделяем элемент
//        spinner3.setSelection(2);
//        // устанавливаем обработчик нажатия
//        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                // показываем позиция нажатого элемента
//                Toast.makeText(getBaseContext(), "Position = Center" + position, Toast.LENGTH_SHORT).show();
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//            }
//        });
    }

    public void changeEvents() {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        // если нет событий
        if (eventsArray.length() == 0) {
            //--- LinearLayout parentLayout ---
            LinearLayout parentLayout = new LinearLayout(this);
            mainLayout.addView(parentLayout);
            createLinearLayout(parentLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);

            //--- TextView noItemText ---
            TextView noItemText = new TextView(this);
            parentLayout.addView(noItemText);
            createTextView(noItemText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, "Нет событий", "#8B939E",18);
        }

        // выводим данные в табличку
        for (int i = 0; i < eventsArray.length(); i++) {
            try {
                createElement(eventsArray.getJSONObject(i)); // создаём элемент

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void showEvents(int num) {
        Thread newThread = new Thread(() -> {
            backgroundWork(Integer.toString(num));
        });

        newThread.start();
        try {
            newThread.join();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            changeEvents();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }



    public void backgroundWork(String num) {
        Runnable runnable = new Runnable() {
            public void run() {
                HttpController httpController = new HttpController();
                try {
                    httpController.MyTicketsHttp(num);
                    httpController.FilterTikc(0);
                    httpController.AllUser();
                    httpController.ChildsForLeader();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    public void showMore(View view) {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        LinearLayout parentLayout = (LinearLayout)view.getParent();
        LinearLayout openLayout = (LinearLayout)parentLayout.getChildAt(2);

        LinearLayout clickLayout = (LinearLayout)view;
        ImageView button = (ImageView)clickLayout.getChildAt(0);

        ViewGroup.LayoutParams params = openLayout.getLayoutParams();

        //openLayout.getVisibility() == View.GONE

        if (openLayout.getHeight() == 0) {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition()); // анимация
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            //openLayout.setVisibility(View.VISIBLE);
            button.setRotation(180);
            clickLayout.setBackgroundColor(Color.parseColor("#F7F5F5"));
        }

        else {
            //openLayout.setVisibility(View.GONE);

            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition());
            params.height = 0;
            button.setRotation(0);
            clickLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        openLayout.setLayoutParams(params);
    }


    // СОЗДАНИЕ НОВОГО ЭЛЕМЕНТА
    public void createElement(JSONObject event) throws JSONException {
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);


        String dateLimit = event.getString("DateLimit");


        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2021-04-24T13:55:29Z", formatter);*/


        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);

        //--- LinearLayout parentLayout ---
        LinearLayout parentLayout = new LinearLayout(this);
        mainLayout.addView(parentLayout);
        createLinearLayout(parentLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout headerLayout ---
        LinearLayout headerLayout = new LinearLayout(this);
        parentLayout.addView(headerLayout);
        headerLayout.setOnClickListener(this::showMore);

        int dpAsPixels = inPixelFromDp(10);
        headerLayout.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);

        createLinearLayout(headerLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- ImageView arrowImage ---
        ImageView arrowImage = new ImageView(this);
        headerLayout.addView(arrowImage);

        dpAsPixels = inPixelFromDp(20);

        // устанавливаем ширину высоту
        ViewGroup.LayoutParams arrowParams = arrowImage.getLayoutParams();
        arrowParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;;
        arrowParams.width = dpAsPixels;
        arrowImage.setLayoutParams(arrowParams);

        arrowImage.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);


        //--- TextView idText ---
        TextView idText = new TextView(this);
        headerLayout.addView(idText);
        idText.setTypeface(open_sans);

        dpAsPixels = inPixelFromDp(100);

        createTextView(idText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, event.getString("Id"), "#4B4A4D",15);


        //--- LinearLayout nameLayout ---
        LinearLayout nameLayout = new LinearLayout(this);
        headerLayout.addView(nameLayout);
        dpAsPixels = inPixelFromDp(5);
        nameLayout.setPadding(dpAsPixels, 0, dpAsPixels, 0);

        createLinearLayout(nameLayout, 0, ViewGroup.LayoutParams.WRAP_CONTENT, 1, Gravity.CENTER, LinearLayout.HORIZONTAL);


        if (event.getString("Criticality").equals("2") || event.getString("Criticality").equals("3")) {
            //--- ImageView fireImage ---
            ImageView fireImage = new ImageView(this);
            nameLayout.addView(fireImage);

            dpAsPixels = inPixelFromDp(20);

            // устанавливаем ширину высоту
            ViewGroup.LayoutParams fireParams = fireImage.getLayoutParams();
            fireParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;;
            fireParams.width = dpAsPixels;
            fireImage.setLayoutParams(fireParams);

            fireImage.setImageResource(R.drawable.ic_baseline_local_fire_department_24);
        }


        //--- TextView nameText ---
        TextView nameText = new TextView(this);
        nameLayout.addView(nameText);
        nameText.setTypeface(open_sans);
        nameText.setMaxLines(1);
        nameText.setEllipsize(TextUtils.TruncateAt.END);
        nameText.setSingleLine(true);
        nameText.setTag(event.getString("Id"));
        nameText.setOnClickListener(this::showEventById);

        createTextView(nameText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, event.getString("Name"), "#2291EA", 15);
        nameText.setTextColor(getResources().getColorStateList(R.color.blue_text_click));

        //--- TextView leftText ---
        TextView leftText = new TextView(this);
        headerLayout.addView(leftText);
        leftText.setTypeface(open_sans);

        dpAsPixels = inPixelFromDp(120);
        String left = getDate(dateLimit);
        String leftColor;
        if (left.equals("Просрочен")) leftColor = "#FF5648";
        else leftColor = "#4B4A4D";
        createTextView(leftText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, left, leftColor,15);


        //--- LinearLayout lineLayout ---
        LinearLayout lineLayout = new LinearLayout(this);
        parentLayout.addView(lineLayout);
        lineLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- openLayout openLayout ---
        LinearLayout openLayout = new LinearLayout(this);
        parentLayout.addView(openLayout);
        // openLayout.setVisibility(View.GONE);

        createLinearLayout(openLayout, ViewGroup.LayoutParams.MATCH_PARENT, 0, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout showMoreLayout ---
        LinearLayout showMoreLayout = new LinearLayout(this);
        openLayout.addView(showMoreLayout);

        int dpAsPixelsH = inPixelFromDp(20);
        int dpAsPixelsV = inPixelFromDp(10);
        showMoreLayout.setPadding(dpAsPixelsH, dpAsPixelsV, dpAsPixelsH, dpAsPixelsV);


        createLinearLayout(showMoreLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);

        if (event.has("Node")) {
            createNewItem("Узел", event.getJSONObject("Node").getString("Name"), "#2291EA", showMoreLayout, event.getJSONObject("Node").getString("Uid"));
        }

        createNewItem("Теги", event.getJSONObject("Tag").getString("Name"), "#4B4A4D", showMoreLayout, "0");

        if ((!event.getString("NumberOfTasks").equals("0")) || (!event.getString("NumberOfCompletedTasks").equals("0"))) {
            createNewItem("Подзадачи", event.getString("NumberOfTasks") + "/" + event.getString("NumberOfCompletedTasks"), "#4B4A4D", showMoreLayout, "0");
        }

        if (event.getString("Status").equals("0")) {
            createNewItem("Статус", "Открыт", "#479696", showMoreLayout, "0");
        }

        if (event.getString("Status").equals("1")) {
            createNewItem("Статус", "В работе", "#bdbf43", showMoreLayout, "0");
        }

        if (event.getString("Status").equals("2")) {
            createNewItem("Статус", "Возвращён в работу", "#bdbf43", showMoreLayout, "0");
        }

        if (event.getString("Status").equals("3")) {
            createNewItem("Статус", "Выполнен", "#479696", showMoreLayout, "0");
        }

        if (event.getString("Status").equals("4")) {
            createNewItem("Статус", "Закрыт", "#479696", showMoreLayout, "0");
        }

        if (event.getString("Status").equals("5")) {
            createNewItem("Статус", "Отклонён", "#FF5648", showMoreLayout, "0");
        }



        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        dpAsPixelsV = inPixelFromDp(10);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER_HORIZONTAL, LinearLayout.VERTICAL);


        if (NumberPage == 1) {
            //--- Button button ---
            MaterialButton button = new MaterialButton(this, null, R.attr.materialButtonOutlinedStyle);
            elemLayout.addView(button);

            createMaterialButton(button, "Завершить работу", "#E6FF5648", "#B0F6E4E4", "#E78181", 16, 14, 2);
        }

        if (NumberPage == 3) {
            //--- Button button ---
            MaterialButton button = new MaterialButton(this, null, R.attr.materialButtonOutlinedStyle);
            elemLayout.addView(button);

            createMaterialButton(button, "Закрыть", "#E6FF5648", "#B0F6E4E4", "#E78181", 16, 14, 2);
        }


        //--- LinearLayout lineOpenLayout ---
        LinearLayout lineOpenLayout = new LinearLayout(this);
        openLayout.addView(lineOpenLayout);
        lineOpenLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineOpenLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);

    }

    public void createMaterialButton(MaterialButton button, String text, String textColor, String backgroundColor, String strokeColor, int cornerRadius, int textSize, int strokeWidth) {
        button.setText(text);
        button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(backgroundColor)));
        if (!strokeColor.equals("null")) button.setStrokeColor(ColorStateList.valueOf(Color.parseColor(strokeColor)));
        int dpAsPixels = inPixelFromDp(strokeWidth);
        button.setStrokeWidth(dpAsPixels);
        button.setTextColor(Color.parseColor(textColor));
        dpAsPixels = inPixelFromDp(cornerRadius);
        button.setCornerRadius(dpAsPixels);
        button.setTextSize(textSize);
        button.setAllCaps(false);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button.getLayoutParams();
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        button.setLayoutParams(params);
    }

    public void createNewItem(String title, String value, String valueColor, LinearLayout showMoreLayout, String nodeUid) {
        Typeface raleway = ResourcesCompat.getFont(this, R.font.raleway_semibold_font);
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);
        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        int dpAsPixelsV = inPixelFromDp(10);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.LEFT, LinearLayout.VERTICAL);


        //--- TextView titleText ---
        TextView titleText = new TextView(this);
        elemLayout.addView(titleText);
        titleText.setTypeface(raleway);

        createTextView(titleText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 0, title, "#4B4A4D",15);


        //--- TextView valueText ---
        TextView valueText = new TextView(this);
        elemLayout.addView(valueText);
        valueText.setTypeface(open_sans);
        dpAsPixelsV = inPixelFromDp(6);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) valueText.getLayoutParams();
        params.setMargins(0, dpAsPixelsV, 0, 0);

        createTextView(valueText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 0, value, valueColor,15);


        if (!nodeUid.equals("0")) {
            valueText.setTag(nodeUid);
            valueText.setOnClickListener(this::showNode);
            valueText.setTextColor(getResources().getColorStateList(R.color.blue_text_click));
        }

    }

    public void showEventById(View view) {
        System.out.println(view.getTag());

        infevents_main infevents_main = new infevents_main();
        Thread newThread = new Thread(() -> {
            infevents_main.backgroundWork(view.getTag().toString());
        });

        newThread.start();
        try {
            newThread.join();
            try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(this, infevents_main.class);
            startActivity(intent);
            //finish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void showNode(View view) {
        System.out.println(view.getTag());

        uzel_uzel uzel_uzel = new uzel_uzel();
        Thread newThread = new Thread(() -> {
            uzel_uzel.backgroundWork(view.getTag().toString());
        });

        newThread.start();
        try {
            newThread.join();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(this, uzel_uzel.class);
            startActivity(intent);
            //finish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public int inPixelFromDp(int sizeInDp) {
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp*scale + 0.5f);
        return dpAsPixels;
    }

    public void createTextView(TextView textView, int width, int height, int gravity, int weight, String text, String textColor, int textSize) {
        // устанавливаем ширину высоту и вес
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        textView.setLayoutParams(params);

        textView.setGravity(gravity);
        textView.setText(text);
        textView.setTextColor(Color.parseColor(textColor));
        textView.setTextSize(textSize);
    }

    public void createLinearLayout(LinearLayout linearLayout, int width, int height, int weight, int gravity, int orientation) {
        // устанавливаем ширину высоту и вес

        linearLayout.setOrientation(orientation);
        linearLayout.setGravity(gravity);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        linearLayout.setLayoutParams(params);
    }

    public String getDate(String dateLimit) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date limitDate = null;
        try {
            limitDate = new Date(format.parse(dateLimit).getTime() + TimeUnit.HOURS.toMillis(3));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date date = new Date();

        Date currentDate = new Date(date.getTime() + TimeUnit.HOURS.toMillis(3)); // получаем текущее время
        //String currentDateString = format.format(currentDate);


        if (limitDate.getTime() < currentDate.getTime()) return "Просрочен";


        long diffInMillies = Math.abs(limitDate.getTime() - currentDate.getTime());
        long diffDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        // long diffMonth = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        return Long.toString(diffDays) + " дн.";
    }

    public void openTopSheetDialog(View view){
        View sheet = findViewById(R.id.top_sheetOpenEvents);
        TopSheetBehavior.from(sheet).setState(TopSheetBehavior.STATE_EXPANDED);

        //Изменения цвета кнопки
        OpenEvent = sheet.findViewById(R.id.OpenButton);
        MyEvent = sheet.findViewById(R.id.MyButton);
        WatchEvent = sheet.findViewById(R.id.button_Watching);
        ControlEvent = sheet.findViewById(R.id.button_Control);
        ExitEvent = sheet.findViewById(R.id.button_Exit);
        MyQuryEvent = sheet.findViewById(R.id.MyQury);
        CloseQuryEvent = sheet.findViewById(R.id.button_QuryExit);

        //Изменения цвета кружка
        OpenEventText = sheet.findViewById(R.id.OpenTextView);
        MyEventText = sheet.findViewById(R.id.MyTextView);
        WatchEventText = sheet.findViewById(R.id.WatchTextView);
        ControlEventText = sheet.findViewById(R.id.ConttextView);
        ExitEventText = sheet.findViewById(R.id.textViewExitButt);
        MyQuryEventText = sheet.findViewById(R.id.textViewMyQure);
        CloseQuryEventText = sheet.findViewById(R.id.textVieQuruExi);

        if(NumberPage == 0)
        {
            //Изменения цвета кнопки
            OpenEvent.setTextColor(Color.parseColor("#3F8AE0"));
            MyEvent.setTextColor(Color.parseColor("#818C99"));
            WatchEvent.setTextColor(Color.parseColor("#818C99"));
            ControlEvent.setTextColor(Color.parseColor("#818C99"));
            ExitEvent.setTextColor(Color.parseColor("#818C99"));
            MyQuryEvent.setTextColor(Color.parseColor("#818C99"));
            CloseQuryEvent.setTextColor(Color.parseColor("#818C99"));

            //Изменения цвета кружка
            OpenEventText.setTextColor(Color.parseColor("#FFFFFF"));
            OpenEventText.setBackground(getDrawable(R.drawable.blueall));
            MyEventText.setTextColor(Color.parseColor("#2291EA"));
            MyEventText.setBackground(getDrawable(R.drawable.blue_border));
            WatchEventText.setTextColor(Color.parseColor("#2291EA"));
            WatchEventText.setBackground(getDrawable(R.drawable.blue_border));
            ControlEventText.setTextColor(Color.parseColor("#2291EA"));
            ControlEventText.setBackground(getDrawable(R.drawable.blue_border));
            ExitEventText.setTextColor(Color.parseColor("#2291EA"));
            ExitEventText.setBackground(getDrawable(R.drawable.blue_border));
            MyQuryEventText.setTextColor(Color.parseColor("#2291EA"));
            MyQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
            CloseQuryEventText.setTextColor(Color.parseColor("#2291EA"));
            CloseQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
            showEvents(NumberPage);
            NamePage = "Открытые события";
            showTickets.setText(NamePage);

        }




        sheet.findViewById(R.id.OpenButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("kok");
                NumberPage = 0;
                if(NumberPage == 0)
                {
                    //Изменения цвета кнопки
                    OpenEvent.setTextColor(Color.parseColor("#3F8AE0"));
                    MyEvent.setTextColor(Color.parseColor("#818C99"));
                    WatchEvent.setTextColor(Color.parseColor("#818C99"));
                    ControlEvent.setTextColor(Color.parseColor("#818C99"));
                    ExitEvent.setTextColor(Color.parseColor("#818C99"));
                    MyQuryEvent.setTextColor(Color.parseColor("#818C99"));
                    CloseQuryEvent.setTextColor(Color.parseColor("#818C99"));

                    //Изменения цвета кружка
                    OpenEventText.setTextColor(Color.parseColor("#FFFFFF"));
                    OpenEventText.setBackground(getDrawable(R.drawable.blueall));
                    MyEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyEventText.setBackground(getDrawable(R.drawable.blue_border));
                    WatchEventText.setTextColor(Color.parseColor("#2291EA"));
                    WatchEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ControlEventText.setTextColor(Color.parseColor("#2291EA"));
                    ControlEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ExitEventText.setTextColor(Color.parseColor("#2291EA"));
                    ExitEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
                    CloseQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    CloseQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
                }

                showEvents(NumberPage);
                NamePage = "Открытые события";
                showTickets.setText(NamePage);
            }
        });



        sheet.findViewById(R.id.MyButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPage = 1;
                if(NumberPage == 1)
                {
                    MyEvent.setTextColor(Color.parseColor("#3F8AE0"));
                    OpenEvent.setTextColor(Color.parseColor("#818C99"));
                    WatchEvent.setTextColor(Color.parseColor("#818C99"));
                    ControlEvent.setTextColor(Color.parseColor("#818C99"));
                    ExitEvent.setTextColor(Color.parseColor("#818C99"));
                    MyQuryEvent.setTextColor(Color.parseColor("#818C99"));
                    CloseQuryEvent.setTextColor(Color.parseColor("#818C99"));

                    OpenEventText.setTextColor(Color.parseColor("#2291EA"));
                    OpenEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyEventText.setTextColor(Color.parseColor("#FFFFFF"));
                    MyEventText.setBackground(getDrawable(R.drawable.blueall));
                    WatchEventText.setTextColor(Color.parseColor("#2291EA"));
                    WatchEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ControlEventText.setTextColor(Color.parseColor("#2291EA"));
                    ControlEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ExitEventText.setTextColor(Color.parseColor("#2291EA"));
                    ExitEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
                    CloseQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    CloseQuryEventText.setBackground(getDrawable(R.drawable.blue_border));

                }

                showEvents(NumberPage);
                NamePage = "Мои события";
                showTickets.setText(NamePage);

            }
        });
        sheet.findViewById(R.id.button_Watching).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPage = 2;
                if(NumberPage == 2)
                {
                    MyEvent.setTextColor(Color.parseColor("#818C99"));
                    OpenEvent.setTextColor(Color.parseColor("#818C99"));
                    WatchEvent.setTextColor(Color.parseColor("#3F8AE0"));
                    ControlEvent.setTextColor(Color.parseColor("#818C99"));
                    ExitEvent.setTextColor(Color.parseColor("#818C99"));
                    MyQuryEvent.setTextColor(Color.parseColor("#818C99"));
                    CloseQuryEvent.setTextColor(Color.parseColor("#818C99"));

                    OpenEventText.setTextColor(Color.parseColor("#2291EA"));
                    OpenEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyEventText.setBackground(getDrawable(R.drawable.blue_border));
                    WatchEventText.setTextColor(Color.parseColor("#FFFFFF"));
                    WatchEventText.setBackground(getDrawable(R.drawable.blueall));
                    ControlEventText.setTextColor(Color.parseColor("#2291EA"));
                    ControlEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ExitEventText.setTextColor(Color.parseColor("#2291EA"));
                    ExitEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
                    CloseQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    CloseQuryEventText.setBackground(getDrawable(R.drawable.blue_border));

                }
                showEvents(NumberPage);
                NamePage = "Наблюдаю";
                showTickets.setText(NamePage);
            }
        });
        sheet.findViewById(R.id.button_Control).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPage = 3;
                if(NumberPage == 3)
                {
                    MyEvent.setTextColor(Color.parseColor("#818C99"));
                    OpenEvent.setTextColor(Color.parseColor("#818C99"));
                    WatchEvent.setTextColor(Color.parseColor("#818C99"));
                    ControlEvent.setTextColor(Color.parseColor("#3F8AE0"));
                    ExitEvent.setTextColor(Color.parseColor("#818C99"));
                    MyQuryEvent.setTextColor(Color.parseColor("#818C99"));
                    CloseQuryEvent.setTextColor(Color.parseColor("#818C99"));

                    OpenEventText.setTextColor(Color.parseColor("#2291EA"));
                    OpenEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyEventText.setBackground(getDrawable(R.drawable.blue_border));
                    WatchEventText.setTextColor(Color.parseColor("#2291EA"));
                    WatchEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ControlEventText.setTextColor(Color.parseColor("#FFFFFF"));
                    ControlEventText.setBackground(getDrawable(R.drawable.blueall));
                    ExitEventText.setTextColor(Color.parseColor("#2291EA"));
                    ExitEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
                    CloseQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    CloseQuryEventText.setBackground(getDrawable(R.drawable.blue_border));

                }
                showEvents(NumberPage);
                System.out.println("kok");
                NamePage = "Контролирую";
                showTickets.setText(NamePage);
            }
        });
        sheet.findViewById(R.id.button_Exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPage = 4;
                if(NumberPage == 4)
                {
                    MyEvent.setTextColor(Color.parseColor("#818C99"));
                    OpenEvent.setTextColor(Color.parseColor("#818C99"));
                    WatchEvent.setTextColor(Color.parseColor("#818C99"));
                    ControlEvent.setTextColor(Color.parseColor("#818C99"));
                    ExitEvent.setTextColor(Color.parseColor("#3F8AE0"));
                    MyQuryEvent.setTextColor(Color.parseColor("#818C99"));
                    CloseQuryEvent.setTextColor(Color.parseColor("#818C99"));

                    OpenEventText.setTextColor(Color.parseColor("#2291EA"));
                    OpenEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyEventText.setBackground(getDrawable(R.drawable.blue_border));
                    WatchEventText.setTextColor(Color.parseColor("#2291EA"));
                    WatchEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ControlEventText.setTextColor(Color.parseColor("#2291EA"));
                    ControlEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ExitEventText.setTextColor(Color.parseColor("#FFFFFF"));
                    ExitEventText.setBackground(getDrawable(R.drawable.blueall));
                    MyQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
                    CloseQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    CloseQuryEventText.setBackground(getDrawable(R.drawable.blue_border));

                }
                showEvents(NumberPage);
                System.out.println("kok");
                NamePage = "Завершённые";
                showTickets.setText(NamePage);
            }
        });
        sheet.findViewById(R.id.MyQury).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPage = 7;
                if(NumberPage == 7)
                {
                    MyEvent.setTextColor(Color.parseColor("#818C99"));
                    OpenEvent.setTextColor(Color.parseColor("#818C99"));
                    WatchEvent.setTextColor(Color.parseColor("#818C99"));
                    ControlEvent.setTextColor(Color.parseColor("#818C99"));
                    ExitEvent.setTextColor(Color.parseColor("#818C99"));
                    MyQuryEvent.setTextColor(Color.parseColor("#3F8AE0"));
                    CloseQuryEvent.setTextColor(Color.parseColor("#818C99"));

                    OpenEventText.setTextColor(Color.parseColor("#2291EA"));
                    OpenEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyEventText.setBackground(getDrawable(R.drawable.blue_border));
                    WatchEventText.setTextColor(Color.parseColor("#2291EA"));
                    WatchEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ControlEventText.setTextColor(Color.parseColor("#2291EA"));
                    ControlEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ExitEventText.setTextColor(Color.parseColor("#2291EA"));
                    ExitEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyQuryEventText.setTextColor(Color.parseColor("#FFFFFF"));
                    MyQuryEventText.setBackground(getDrawable(R.drawable.blueall));
                    CloseQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    CloseQuryEventText.setBackground(getDrawable(R.drawable.blue_border));

                }
                showEvents(NumberPage);
                System.out.println("kok");
                NamePage = "Мои запросы";
                showTickets.setText(NamePage);
            }
        });
        sheet.findViewById(R.id.button_QuryExit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPage = 6;
                if(NumberPage == 6)
                {
                    MyEvent.setTextColor(Color.parseColor("#818C99"));
                    OpenEvent.setTextColor(Color.parseColor("#818C99"));
                    WatchEvent.setTextColor(Color.parseColor("#818C99"));
                    ControlEvent.setTextColor(Color.parseColor("#818C99"));
                    ExitEvent.setTextColor(Color.parseColor("#818C99"));
                    MyQuryEvent.setTextColor(Color.parseColor("#818C99"));
                    CloseQuryEvent.setTextColor(Color.parseColor("#3F8AE0"));

                    OpenEventText.setTextColor(Color.parseColor("#2291EA"));
                    OpenEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyEventText.setBackground(getDrawable(R.drawable.blue_border));
                    WatchEventText.setTextColor(Color.parseColor("#2291EA"));
                    WatchEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ControlEventText.setTextColor(Color.parseColor("#2291EA"));
                    ControlEventText.setBackground(getDrawable(R.drawable.blue_border));
                    ExitEventText.setTextColor(Color.parseColor("#2291EA"));
                    ExitEventText.setBackground(getDrawable(R.drawable.blue_border));
                    MyQuryEventText.setTextColor(Color.parseColor("#2291EA"));
                    MyQuryEventText.setBackground(getDrawable(R.drawable.blue_border));
                    CloseQuryEventText.setTextColor(Color.parseColor("#FFFFFF"));
                    CloseQuryEventText.setBackground(getDrawable(R.drawable.blueall));

                }
                showEvents(NumberPage);
                System.out.println("kok");
                NamePage = "Запрошенные";
                showTickets.setText(NamePage);
            }
        });

    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/pictures" + "/tmp_screen.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    public void openBottomSheetDialog(View view){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_open_events);
        TegLiner = bottomSheetDialog.findViewById(R.id.TegLinerTow);
        StatusLiner = bottomSheetDialog.findViewById(R.id.StatusLinerTow);
        bottomSheetDialog.findViewById(R.id.ExitButtonFilterOpenEve).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

        cardSort = bottomSheetDialog.findViewById(R.id.CardSort);
        cardDep = bottomSheetDialog.findViewById(R.id.CardDep);
        cardStatus = bottomSheetDialog.findViewById(R.id.CardStatus);
        cardTeg = bottomSheetDialog.findViewById(R.id.CardTeg);
//        cardOring = bottomSheetDialog.findViewById(R.id.CardOring);
//        cardFIO = bottomSheetDialog.findViewById(R.id.CardFIO);
//        cardRole = bottomSheetDialog.findViewById(R.id.CardRole);
//        cardNameEvent = bottomSheetDialog.findViewById(R.id.CardNameEvent);
//        cardFQDN = bottomSheetDialog.findViewById(R.id.CardFQDN);
//        cardDateOpenTo = bottomSheetDialog.findViewById(R.id.CardDateOpenTo);
//        cardDateOpenDo = bottomSheetDialog.findViewById(R.id.CardDateOpenDo);
//        cardDateCloseTo = bottomSheetDialog.findViewById(R.id.CardDateCloseTo);
//        cardDateCloseDo = bottomSheetDialog.findViewById(R.id.CardDateCloseDo);
        ArrayAdapter adapter1 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataSort);
        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, datadep);
        ArrayAdapter adapter3 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataStatus);
        ArrayAdapter adapter4 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataTag);
//        ArrayAdapter adapterRole = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataRole);

//        CreteButtonFitr(linerPop,"pop");
        cardSort.setAdapter(adapter1);
//        cardSort.setText(cardSort.getAdapter().getItem(2).toString(), false);
        cardSort.setThreshold(1);
        cardDep.setAdapter(adapter2);
        cardDep.setThreshold(1);
        cardStatus.setAdapter(adapter3);
        cardStatus.setThreshold(1);


        cardStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = String.valueOf(parent.getItemAtPosition(position));
                CreteButtonFitr(StatusLiner, text);

            }
        });


        cardTeg.setAdapter(adapter4);
        cardTeg.setThreshold(1);
        cardTeg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = String.valueOf(parent.getItemAtPosition(position));
                CreteButtonFitr(TegLiner, text);
            }
        });


//        cardRole.setAdapter(adapterRole);
//        cardRole.setThreshold(1);
//        cardRole.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

//        bottomSheetDialog.findViewById(R.id.CardRole).setOnItemClickListener(new AdapterView.OnItemClickListener(){
//
//            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//                String text = (String) parent.getItemAtPosition(position);
//                CreteButtonFitr(linerPop,text);
//            }
//        });


//        bottomSheetDialog.findViewById(R.id.CardDateOpenTo).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NumIf = 1;
//                DialogFragment dataPiker = new date_pick();
//                dataPiker.show(getSupportFragmentManager(), "DataPiker");
//            }
//        });
//        bottomSheetDialog.findViewById(R.id.CardDateOpenDo).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NumIf = 2;
//                DialogFragment dataPiker = new date_pick();
//                dataPiker.show(getSupportFragmentManager(), "DataPiker");
//            }
//        });
//
//        bottomSheetDialog.findViewById(R.id.CardDateCloseTo).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NumIf = 3;
//                DialogFragment dataPiker = new date_pick();
//                dataPiker.show(getSupportFragmentManager(), "DataPiker");
//            }
//        });
//
//        bottomSheetDialog.findViewById(R.id.CardDateCloseDo).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NumIf = 4;
//                DialogFragment dataPiker = new date_pick();
//                dataPiker.show(getSupportFragmentManager(), "DataPiker");
//            }
//        });


        bottomSheetDialog.findViewById(R.id.ClearFilterOpenEve).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardSort.setText(null);
                cardDep.setText(null);
                cardStatus.setText(null);
                cardTeg.setText(null);
//                cardOring.setText(null);
//                cardFIO.setText(null);
//                cardRole.setText(null);
//                cardNameEvent.setText(null);
//                cardFQDN.setText(null);
//                cardDateOpenTo.setText(null);
//                cardDateOpenDo.setText(null);
//                cardDateCloseTo.setText(null);
//                cardDateCloseDo.setText(null);
            }
        });
        bottomSheetDialog.show();
    }

//    @Override
//    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.YEAR,year);
//        c.set(Calendar.MONTH,month);
//        c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
//        if(NumIf == 1){
//            String currentDateString= DateFormat.getDateInstance().format(c.getTime());
//            cardDateOpenTo.setText(currentDateString);
//        }
//        else if(NumIf == 2){
//            String currentDateString= DateFormat.getDateInstance().format(c.getTime());
//            cardDateOpenDo.setText(currentDateString);
//        }
//        else if(NumIf == 3){
//            String currentDateString= DateFormat.getDateInstance().format(c.getTime());
//            cardDateCloseTo.setText(currentDateString);
//        }
//        else if(NumIf == 4){
//            String currentDateString= DateFormat.getDateInstance().format(c.getTime());
//            cardDateCloseDo.setText(currentDateString);
//        }
//
//
//
//    }




    public void openBottomSheetDialogAddEvents(View view){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_newevents);
        bottomSheetDialog.show();

        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, AllUser);
        ArrayAdapter adapter4 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataTag);

        ShowAllSet = bottomSheetDialog.findViewById(R.id.AllSeting);
        cardF = bottomSheetDialog.findViewById(R.id.CardFIOo);
        cardF.setAdapter(adapter2);
        cardF.setThreshold(1);
        cardCritp = bottomSheetDialog.findViewById(R.id.CardCrit);
        cardFQDN = bottomSheetDialog.findViewById(R.id.CardFQDN);
        cardTeg1 = bottomSheetDialog.findViewById(R.id.CardTegS);
        cardTeg1.setAdapter(adapter4);
        cardTeg1.setThreshold(1);
        cardDep1 = bottomSheetDialog.findViewById(R.id.CardDepT);
        cardOrigin1 = bottomSheetDialog.findViewById(R.id.CardOrigin);
        cardDeadLine = bottomSheetDialog.findViewById(R.id.CardDateOpenToT);
        cardDM = bottomSheetDialog.findViewById(R.id.CardDangenMaster);
        cardDic = bottomSheetDialog.findViewById(R.id.cardochka5Dic);

        cardDangenMMen = bottomSheetDialog.findViewById(R.id.CardDangenMasterMen);
        cardAddsis = bottomSheetDialog.findViewById(R.id.CardAddSis);
        CardCow = bottomSheetDialog.findViewById(R.id.CardCoworck);
        cardMone = bottomSheetDialog.findViewById(R.id.CardMony);
        cardIp = bottomSheetDialog.findViewById(R.id.CardIP);
        cardBedBoy = bottomSheetDialog.findViewById(R.id.CardBadBoy);
        cardFQDNUzel = bottomSheetDialog.findViewById(R.id.CardFqdnUzel);
        cardDateOpenTotot = bottomSheetDialog.findViewById(R.id.CardDateOpenTotot);
        CardTimeOpenDo = bottomSheetDialog.findViewById(R.id.CardTimeOpenDo);

        LinerFiltetop = bottomSheetDialog.findViewById(R.id.LinerFilter);

        bottomSheetDialog.findViewById(R.id.ExitButtonFilterOpenEve).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });


        bottomSheetDialog.findViewById(R.id.AllSeting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                if(checked){
                    LinerFiltetop.setVisibility(View.VISIBLE);
                }
                else {
                    LinerFiltetop.setVisibility(View.GONE);
                }
            }
        });

        bottomSheetDialog.findViewById(R.id.ClearFilterOpenEve).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardF.setText(null);
                cardCritp.setText(null);
                cardFQDN.setText(null);
                cardTeg1.setText(null);
                cardDep1.setText(null);
                cardOrigin1.setText(null);
                cardDeadLine.setText(null);
                cardDM.setText(null);
                cardDic.setText(null);
                cardDangenMMen.setText(null);
                cardAddsis.setText(null);
                CardCow.setText(null);
                cardMone.setText(null);
                cardIp.setText(null);
                cardBedBoy.setText(null);
                cardFQDNUzel.setText(null);
                cardDateOpenTotot.setText(null);
                CardTimeOpenDo.setText(null);
            }
        });




    }


    public void CreteButtonFitr(LinearLayout liner, String text)
    {
        MaterialButton button = new MaterialButton(this, null, R.attr.materialButtonOutlinedStyle);
        liner.addView(button);
        button.setText(text);
        button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#000FFFFF")));
        button.setStrokeColor(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
        button.setTextColor(Color.parseColor("#3F8AE0"));
        button.setAllCaps(false);
        int dp = inPixelFromDp(25);
        button.setCornerRadius(dp);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button.getLayoutParams();
        params.height = inPixelFromDp(41);
        params.weight = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.setMarginEnd(inPixelFromDp(5));
        button.setLayoutParams(params);
        button.setIcon(ContextCompat.getDrawable(this,R.drawable.ic_baseline_close_24));
        button.setIconTintResource(R.color.blueFilter);
        button.setIconSize(inPixelFromDp(16));
        button.setIconGravity(MaterialButton.ICON_GRAVITY_END);
        button.setStrokeColorResource(R.color.blueFilter);

    }





//    open_events_filter sheet = new open_events_filter();

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.ButtonOpenFilterEvents:
//                try {
//                    sheet.show(this);
//                } catch (Exception e) {
//
//                }
////                sheet.show(this);
//                break;
//            default:
//                break;
//
//        }
//        switch (v.getId()) {
//            case R.id.cardochka:
//                arrayList_sort = new ArrayList<>();
//                arrayList_sort.add("Сначала срочные");
//                arrayList_sort.add("По порядку ID");
//                arrayList_sort.add("По названию (А-Я)");
//                arrayList_sort.add("По времени");
//                arrayAdapter_sort = new ArrayAdapter<>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, arrayList_sort);
//                cardochka.setAdapter(arrayAdapter_sort);
//                cardochka.setThreshold(1);
//                break;
//            default:
//                break;
//        }
//        switch (v.getId()) {
//            case R.id.ExitButtonFilterOpenEve:
//                sheet.dismiss();
//                break;
//            default:
//                break;
//        }
//    }
}
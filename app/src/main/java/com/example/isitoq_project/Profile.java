package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.isitoq_project.servercontroller.HttpController;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class Profile extends AppCompatActivity {

    public static String USerId;
    public static String USerName;
    public static String USerMail;
    public static String USerPhone;
    public static String Department;
    public static String PhotoPop;
    TextView name;
    TextView mail;
    TextView phone;
    TextView Depo;
    ImageButton menu;
    CircleImageView PhotoProf;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // backgroundWork();

        name = (TextView) findViewById(R.id.NameText);
        mail = (TextView) findViewById(R.id.TextMail);
        phone = (TextView) findViewById(R.id.Phone);
        Depo = (TextView) findViewById(R.id.NameDep);
//        bit = StringToBitMap(PhotoPop);
        PhotoProf = (CircleImageView) findViewById(R.id.ProfilePhoto);
        LoadImage(PhotoPop);
        name.setText(USerName);
        mail.setText(USerMail);
        phone.setText(USerPhone);
        Depo.setText(Department);

        menu = (ImageButton) findViewById(R.id.Profile_menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Main_page.NumPageEliment = 2;
                    takeScreenshot();
                    Intent intent = new Intent(Profile.this, Main_page.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_sidein, R.anim.left_slideout);
                    finish();
                } catch (Exception e) {

                }
            }
        });

    }

    public void LoadImage(String url){
        Picasso.with(this).load(url).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(PhotoProf, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });
    }

//    public Bitmap StringToBitMap(String encodedString){
//        try{
//            byte [] encodeByte = Base64.decode(encodedString,Base64.URL_SAFE);
//            InputStream inputStream  = new ByteArrayInputStream(encodeByte);
//            Bitmap bitmapq  = BitmapFactory.decodeStream(inputStream);
//            return bitmapq;
//        }
//        catch(Exception e){
//            e.getMessage();
//            return null;
//        }
//    }

    public void backgroundWork() {
        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    HttpController httpController = new HttpController();
                    httpController.ProfileHttp();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/pictures" + "/tmp_screen.jpg";
            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            File imageFile = new File(mPath);
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }
}
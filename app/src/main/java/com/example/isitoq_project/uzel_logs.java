package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

public class uzel_logs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uzel_logs);

        createElement();
        createElement();
        createElement();
    }

    // СОЗДАНИЕ НОВОГО ЭЛЕМЕНТА
    public void createElement() {
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);

        String dateString = "";
        String dtStart = "2021-05-15T09:27:37.356Z";
        SimpleDateFormat beforeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat afterFormat = new SimpleDateFormat("dd.MM.yyyy");


        Date date = null;
        try {
            date = beforeFormat.parse(dtStart);
            dateString = afterFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);

        //--- LinearLayout parentLayout ---
        LinearLayout parentLayout = new LinearLayout(this);
        mainLayout.addView(parentLayout);
        createLinearLayout(parentLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout headerLayout ---
        LinearLayout headerLayout = new LinearLayout(this);
        parentLayout.addView(headerLayout);

        int dpAsPixelsV = inPixelFromDp(10);
        int dpAsPixelsH = inPixelFromDp(20);
        headerLayout.setPadding(dpAsPixelsH, dpAsPixelsV, dpAsPixelsH, dpAsPixelsV);

        createLinearLayout(headerLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- TextView dateText ---
        TextView dateText = new TextView(this);
        headerLayout.addView(dateText);
        dateText.setTypeface(open_sans);

        int dpAsPixels = inPixelFromDp(150);

        createTextView(dateText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, dateString, "#2291EA",15);


        //--- LinearLayout nameLayout ---
        LinearLayout nameLayout = new LinearLayout(this);
        headerLayout.addView(nameLayout);
        dpAsPixels = inPixelFromDp(5);
        nameLayout.setPadding(dpAsPixels, 0, dpAsPixels, 0);

        createLinearLayout(nameLayout, 0, ViewGroup.LayoutParams.WRAP_CONTENT, 1, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- TextView nameText ---
        TextView nameText = new TextView(this);
        nameLayout.addView(nameText);
        nameText.setTypeface(open_sans);
        nameText.setMaxLines(1);
        nameText.setEllipsize(TextUtils.TruncateAt.END);
        nameText.setSingleLine(true);

        createTextView(nameText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, "Пример", "#4B4A4D", 15);



        //--- LinearLayout lineLayout ---
        LinearLayout lineLayout = new LinearLayout(this);
        parentLayout.addView(lineLayout);
        lineLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


    }

    public int inPixelFromDp(int sizeInDp) {
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp*scale + 0.5f);
        return dpAsPixels;
    }

    public void createTextView(TextView textView, int width, int height, int gravity, int weight, String text, String textColor, int textSize) {
        // устанавливаем ширину высоту и вес
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        textView.setLayoutParams(params);

        textView.setGravity(gravity);
        textView.setText(text);
        textView.setTextColor(Color.parseColor(textColor));
        textView.setTextSize(textSize);
    }

    public void createLinearLayout(LinearLayout linearLayout, int width, int height, int weight, int gravity, int orientation) {
        // устанавливаем ширину высоту и вес

        linearLayout.setOrientation(orientation);
        linearLayout.setGravity(gravity);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        linearLayout.setLayoutParams(params);
    }
}
package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.topsheetlib.TopSheetBehavior;
import com.example.topsheetlib.TopSheetDialog;
import com.example.isitoq_project.servercontroller.HttpController;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class infevents_main extends AppCompatActivity {

    public static String Name = "";
    public static String Department = "";
    public static String ResponsibleUser = "";
    public static String Criticality = "0";
    public static String DateOpen = "";
    public static String DateLimit = "";
    public static String DateIncident = "";
    public static String Source = "";

    public static String RegardSystem = "";
    public static String LostExpectancy = "";
    public static String LostActual = "";
    public static String Node = "";
    public static String AttackersIP = "";
    public static String AttackersUserName = "";
    public static String AttackersFQDN = "";
    public static String Description = "";
    private ImageButton More;
    private ImageButton addItem;
    private Button showMore;
    private MaterialButton Main;
    private MaterialButton Task;
    private MaterialButton Part;
    private MaterialButton Otch;
    private MaterialButton Logi;
    public static int NumberPageLogi = 0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infevents_main);

        More = findViewById(R.id.more_menuInfer);
        More.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main_page.NumPageEliment = 10;
                takeScreenshot();
                Intent intent = new Intent(infevents_main.this, Main_page.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_sidein, R.anim.left_slideout);
                finish();
            }
        });

        addItem = findViewById(R.id.add_itemIfner);
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheetDialogAddEventsFromInf(v);

            }
        });
        showMore = findViewById(R.id.buttonInf);
        showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowTopSheet(v);
            }
        });
        setDataToPage();
    }

    public void ShowTopSheet(View view){
        View sheet = findViewById(R.id.top_sheetIfner);
        TopSheetBehavior.from(sheet).setState(TopSheetBehavior.STATE_EXPANDED);
        Main = sheet.findViewById(R.id.button_Osn);
        Task = sheet.findViewById(R.id.button_zadachi);
        Part = sheet.findViewById(R.id.button_uchastniki);
        Otch = sheet.findViewById(R.id.button_otchet);
        Logi = sheet.findViewById(R.id.button_logi);

        if(NumberPageLogi == 0)
        {
            //Изменения цвета кнопки
            Main.setTextColor(Color.parseColor("#3F8AE0"));
            Task.setTextColor(Color.parseColor("#818C99"));
            Part.setTextColor(Color.parseColor("#818C99"));
            Otch.setTextColor(Color.parseColor("#818C99"));
            Logi.setTextColor(Color.parseColor("#818C99"));


        }


        sheet.findViewById(R.id.button_Osn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("kok");
                NumberPageLogi = 0;
                if(NumberPageLogi == 0)
                {
                    //Изменения цвета кнопки
                    Main.setTextColor(Color.parseColor("#3F8AE0"));
                    Main.setIconTint(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
                    Task.setTextColor(Color.parseColor("#818C99"));
                    Task.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Part.setTextColor(Color.parseColor("#818C99"));
                    Part.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Otch.setTextColor(Color.parseColor("#818C99"));
                    Otch.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Logi.setTextColor(Color.parseColor("#818C99"));
                    Logi.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));

                }


            }
        });



        sheet.findViewById(R.id.button_zadachi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPageLogi = 1;
                if(NumberPageLogi == 1)
                {
                    Main.setTextColor(Color.parseColor("#818C99"));
                    Main.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Task.setTextColor(Color.parseColor("#3F8AE0"));
                    Task.setIconTint(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
                    Part.setTextColor(Color.parseColor("#818C99"));
                    Part.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Otch.setTextColor(Color.parseColor("#818C99"));
                    Otch.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Logi.setTextColor(Color.parseColor("#818C99"));
                    Logi.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));


                }


            }
        });
        sheet.findViewById(R.id.button_uchastniki).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPageLogi = 2;
                if(NumberPageLogi == 2)
                {
                    Main.setTextColor(Color.parseColor("#818C99"));
                    Main.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Task.setTextColor(Color.parseColor("#818C99"));
                    Task.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Part.setTextColor(Color.parseColor("#3F8AE0"));
                    Part.setIconTint(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
                    Otch.setTextColor(Color.parseColor("#818C99"));
                    Otch.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Logi.setTextColor(Color.parseColor("#818C99"));
                    Logi.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));


                }

            }
        });
        sheet.findViewById(R.id.button_otchet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPageLogi = 3;
                if(NumberPageLogi == 3)
                {
                    Main.setTextColor(Color.parseColor("#818C99"));
                    Main.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Task.setTextColor(Color.parseColor("#818C99"));
                    Task.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Part.setTextColor(Color.parseColor("#818C99"));
                    Part.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Otch.setTextColor(Color.parseColor("#3F8AE0"));
                    Otch.setIconTint(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
                    Logi.setTextColor(Color.parseColor("#818C99"));
                    Logi.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));

                }

            }
        });
        sheet.findViewById(R.id.button_logi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPageLogi = 4;
                if(NumberPageLogi == 4)
                {
                    Main.setTextColor(Color.parseColor("#818C99"));
                    Main.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Task.setTextColor(Color.parseColor("#818C99"));
                    Task.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Part.setTextColor(Color.parseColor("#818C99"));
                    Part.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Otch.setTextColor(Color.parseColor("#818C99"));
                    Otch.setIconTint(ColorStateList.valueOf(Color.parseColor("#818C99")));
                    Logi.setTextColor(Color.parseColor("#3F8AE0"));
                    Logi.setIconTint(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));

                }

            }
        });


    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/pictures" + "/tmp_screen.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    public void openBottomSheetDialogAddEventsFromInf(View view){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_newevents);
        bottomSheetDialog.show();
    }

    public void setDataToPage() {
        TextView name = (TextView) findViewById(R.id.name);
        TextView department = (TextView) findViewById(R.id.department);
        TextView responsibleUser = (TextView) findViewById(R.id.responsibleUser);
        TextView source = (TextView) findViewById(R.id.source);
        TextView criticality = (TextView) findViewById(R.id.criticality);
        TextView dateOpen = (TextView) findViewById(R.id.dateOpen);
        TextView dateLimit = (TextView) findViewById(R.id.dateLimit);
        TextView dateIncident = (TextView) findViewById(R.id.dateIncident);

        TextView regardSystem = (TextView) findViewById(R.id.regardSystem);
        TextView lostExpectancy = (TextView) findViewById(R.id.lostExpectancy);
        TextView lostActual = (TextView) findViewById(R.id.lostActual);
        TextView node = (TextView) findViewById(R.id.node);
        TextView attackersIP = (TextView) findViewById(R.id.attackersIP);
        TextView attackersUserName = (TextView) findViewById(R.id.attackersUserName);
        TextView attackersFQDN = (TextView) findViewById(R.id.attackersFQDN);
        TextView description = (TextView) findViewById(R.id.description);

        name.setText(Name);
        department.setText(Department);
        responsibleUser.setText(ResponsibleUser);
        if(!Source.equals("")) source.setText(Source);

        if (Criticality.equals("0")) criticality.setText("Низкий");
        if (Criticality.equals("1")) criticality.setText("Средний");
        if (Criticality.equals("2")) criticality.setText("Высокий");
        if (Criticality.equals("3")) criticality.setText("Критичный");

        dateOpen.setText(convertDate(DateOpen));
        dateLimit.setText(convertDate(DateLimit));
        if(!DateIncident.equals("")) dateIncident.setText(convertDate(DateIncident));

        if(!RegardSystem.equals("")) regardSystem.setText(RegardSystem);
        if(!LostExpectancy.equals("")) lostExpectancy.setText(LostExpectancy);
        if(!LostActual.equals("")) lostActual.setText(LostActual);
        if(!Node.equals("")) node.setText(Node);
        if(!AttackersIP.equals("")) attackersIP.setText(AttackersIP);
        if(!AttackersUserName.equals("")) attackersUserName.setText(AttackersUserName);
        if(!AttackersFQDN.equals("")) attackersFQDN.setText(AttackersFQDN);
        if(!Description.equals("")) description.setText(Description);
    }



    public void backgroundWork(String id) {
        Runnable runnable = new Runnable() {
            public void run() {
                HttpController httpController = new HttpController();
                try {
                    httpController.TicketHttp(id);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public String convertDate(String dateBefore) {
        String dateString = "";
        SimpleDateFormat beforeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat afterFormat = new SimpleDateFormat("dd.MM.yyyy' в 'HH:mm:ss");


        Date date = null;
        try {
            date = beforeFormat.parse(dateBefore);
            Date newDate = new Date(date.getTime() + TimeUnit.HOURS.toMillis(3));
            dateString = afterFormat.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateString;
    }

    public void showMore(View view) {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        CardView clickLayout = (CardView) view;
        LinearLayout parentLayout = (LinearLayout)clickLayout.getChildAt(0);
        LinearLayout openLayout = (LinearLayout)parentLayout.getChildAt(1);

        LinearLayout headerLayout = (LinearLayout)parentLayout.getChildAt(0);
        LinearLayout innerLayout = (LinearLayout)headerLayout.getChildAt(0);
        ImageButton button = (ImageButton)innerLayout.getChildAt(1);

        ViewGroup.LayoutParams params = openLayout.getLayoutParams();

        if (openLayout.getHeight() == 0) {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition()); // анимация
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            button.setRotation(90);
            button.setImageTintList(ColorStateList.valueOf(Color.parseColor("#B8C1CC")));
        }

        else {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition());
            params.height = 0;
            button.setRotation(0);
            button.setImageTintList(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
        }

        openLayout.setLayoutParams(params);
    }
}
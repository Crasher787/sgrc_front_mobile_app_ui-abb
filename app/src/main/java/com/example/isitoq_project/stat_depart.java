package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class stat_depart extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stat_depart);

        createElement();
    }

    public void showMore(View view) {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        LinearLayout parentLayout = (LinearLayout)view.getParent();
        LinearLayout openLayout = (LinearLayout)parentLayout.getChildAt(2);

        LinearLayout clickLayout = (LinearLayout)view;
        ImageView button = (ImageView)clickLayout.getChildAt(0);

        ViewGroup.LayoutParams params = openLayout.getLayoutParams();

        //openLayout.getVisibility() == View.GONE

        if (openLayout.getHeight() == 0) {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition()); // анимация
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            //openLayout.setVisibility(View.VISIBLE);
            button.setRotation(180);
            clickLayout.setBackgroundColor(Color.parseColor("#F7F5F5"));
        }

        else {
            //openLayout.setVisibility(View.GONE);

            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition());
            params.height = 0;
            button.setRotation(0);
            clickLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        openLayout.setLayoutParams(params);
    }


    // СОЗДАНИЕ НОВОГО ЭЛЕМЕНТА
    public void createElement() {
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);

        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);

        //--- LinearLayout parentLayout ---
        LinearLayout parentLayout = new LinearLayout(this);
        mainLayout.addView(parentLayout);
        createLinearLayout(parentLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout headerLayout ---
        LinearLayout headerLayout = new LinearLayout(this);
        parentLayout.addView(headerLayout);
        headerLayout.setOnClickListener(this::showMore);

        int dpAsPixels = inPixelFromDp(10);
        headerLayout.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);

        createLinearLayout(headerLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- ImageView arrowImage ---
        ImageView arrowImage = new ImageView(this);
        headerLayout.addView(arrowImage);

        dpAsPixels = inPixelFromDp(20);

        // устанавливаем ширину высоту
        ViewGroup.LayoutParams arrowParams = arrowImage.getLayoutParams();
        arrowParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;;
        arrowParams.width = dpAsPixels;
        arrowImage.setLayoutParams(arrowParams);

        arrowImage.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);


        //--- LinearLayout nameLayout ---
        LinearLayout nameLayout = new LinearLayout(this);
        headerLayout.addView(nameLayout);
        dpAsPixels = inPixelFromDp(30);
        nameLayout.setPadding(dpAsPixels, 0, 0, 0);

        createLinearLayout(nameLayout, 0, ViewGroup.LayoutParams.WRAP_CONTENT, 1, Gravity.LEFT, LinearLayout.HORIZONTAL);


        //--- TextView nameText ---
        TextView nameText = new TextView(this);
        nameLayout.addView(nameText);
        nameText.setTypeface(open_sans);
        nameText.setMaxLines(1);
        nameText.setEllipsize(TextUtils.TruncateAt.END);
        nameText.setSingleLine(true);

        createTextView(nameText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, "Ак Барс ДИП", "#4B4A4D", 15);


        //--- LinearLayout lineLayout ---
        LinearLayout lineLayout = new LinearLayout(this);
        parentLayout.addView(lineLayout);
        lineLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);

        //--- openLayout parentOpenLayout ---
        LinearLayout parentOpenLayout = new LinearLayout(this);
        parentLayout.addView(parentOpenLayout);

        createLinearLayout(parentOpenLayout, ViewGroup.LayoutParams.MATCH_PARENT, 0, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        createNewRow(parentOpenLayout);
    }


    public void createNewRow(LinearLayout parentOpenLayout) {
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);

        //--- LinearLayout headerLayout ---
        LinearLayout headerLayout = new LinearLayout(this);
        parentOpenLayout.addView(headerLayout);
        headerLayout.setOnClickListener(this::showMore);

        int dpAsPixels = inPixelFromDp(10);
        int dpAsPixelsS = inPixelFromDp(40);
        headerLayout.setPadding(dpAsPixelsS, dpAsPixels, dpAsPixels, dpAsPixels);

        createLinearLayout(headerLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- ImageView arrowImage ---
        ImageView arrowImage = new ImageView(this);
        headerLayout.addView(arrowImage);

        dpAsPixels = inPixelFromDp(20);

        // устанавливаем ширину высоту
        ViewGroup.LayoutParams arrowParams = arrowImage.getLayoutParams();
        arrowParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;;
        arrowParams.width = dpAsPixels;
        arrowImage.setLayoutParams(arrowParams);

        arrowImage.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);


        //--- LinearLayout nameLayout ---
        LinearLayout nameLayout = new LinearLayout(this);
        headerLayout.addView(nameLayout);
        dpAsPixels = inPixelFromDp(30);
        nameLayout.setPadding(dpAsPixels, 0, 0, 0);

        createLinearLayout(nameLayout, 0, ViewGroup.LayoutParams.WRAP_CONTENT, 1, Gravity.LEFT, LinearLayout.HORIZONTAL);


        //--- TextView nameText ---
        TextView nameText = new TextView(this);
        nameLayout.addView(nameText);
        nameText.setTypeface(open_sans);
        nameText.setMaxLines(1);
        nameText.setEllipsize(TextUtils.TruncateAt.END);
        nameText.setSingleLine(true);

        createTextView(nameText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, "Департамент разработки ПО", "#4B4A4D", 15);


        //--- LinearLayout lineLayout ---
        LinearLayout lineLayout = new LinearLayout(this);
        parentOpenLayout.addView(lineLayout);
        lineLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);

        //--- openLayout openLayout ---
        LinearLayout openLayout = new LinearLayout(this);
        parentOpenLayout.addView(openLayout);
        // openLayout.setVisibility(View.GONE);

        createLinearLayout(openLayout, ViewGroup.LayoutParams.MATCH_PARENT, 0, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout showMoreLayout ---
        LinearLayout showMoreLayout = new LinearLayout(this);
        openLayout.addView(showMoreLayout);

        int dpAsPixelsH = inPixelFromDp(30);
        int dpAsPixelsV = inPixelFromDp(20);
        showMoreLayout.setPadding(dpAsPixelsH, dpAsPixelsV, dpAsPixelsH, dpAsPixelsV);

        createLinearLayout(showMoreLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        createNewItem("В работе", "16", true, showMoreLayout);

        //--- LinearLayout lineBlueLayout ---
        LinearLayout lineBlueLayout = new LinearLayout(this);
        showMoreLayout.addView(lineBlueLayout);
        lineBlueLayout.setBackgroundColor(Color.parseColor("#2291EA"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineBlueLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);

        createNewItem("Возвращено в работу", "5", false, showMoreLayout);
        createNewItem("Просрочено", "6", false, showMoreLayout);
        createNewItem("На проверке", "1", false, showMoreLayout);


        //--- LinearLayout lineOpenLayout ---
        LinearLayout lineOpenLayout = new LinearLayout(this);
        openLayout.addView(lineOpenLayout);
        lineOpenLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineOpenLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);

    }


    public void createNewItem(String title, String value, boolean main, LinearLayout showMoreLayout) {
        Typeface font;
        if (main) font = ResourcesCompat.getFont(this, R.font.raleway_semibold_font);
        else font = ResourcesCompat.getFont(this, R.font.open_sans_regular);

        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        int dpAsPixelsV = inPixelFromDp(5);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.LEFT, LinearLayout.HORIZONTAL);


        //--- TextView titleText ---
        TextView titleText = new TextView(this);
        elemLayout.addView(titleText);
        titleText.setTypeface(font);

        int dpAsPixels = inPixelFromDp(2);
        titleText.setPadding(dpAsPixels, 0, 0, 0);

        createTextView(titleText, 0, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 1, title, "#4B4A4D",15);


        //--- TextView valueText ---
        TextView valueText = new TextView(this);
        elemLayout.addView(valueText);
        valueText.setTypeface(font);
        dpAsPixels = inPixelFromDp(50);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) valueText.getLayoutParams();
        params.setMargins(0, 0, dpAsPixels, 0);

        dpAsPixels = inPixelFromDp(40);

        createTextView(valueText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER, 0, value, "#4B4A4D",15);
    }


    public int inPixelFromDp(int sizeInDp) {
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp*scale + 0.5f);
        return dpAsPixels;
    }

    public void createTextView(TextView textView, int width, int height, int gravity, int weight, String text, String textColor, int textSize) {
        // устанавливаем ширину высоту и вес
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        textView.setLayoutParams(params);

        textView.setGravity(gravity);
        textView.setText(text);
        textView.setTextColor(Color.parseColor(textColor));
        textView.setTextSize(textSize);
    }


    public void createLinearLayout(LinearLayout linearLayout, int width, int height, int weight, int gravity, int orientation) {
        // устанавливаем ширину высоту и вес

        linearLayout.setOrientation(orientation);
        linearLayout.setGravity(gravity);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        linearLayout.setLayoutParams(params);
    }
}
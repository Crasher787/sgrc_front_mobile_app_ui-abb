package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.isitoq_project.servercontroller.HttpController;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class irp_page extends AppCompatActivity implements View.OnClickListener {

    ImageButton profile;
    ImageButton menu;
    LinearLayout leftMarker;
    LinearLayout centerMarker;
    LinearLayout rightMarker;
    public static int NumberOfClosed = 1;
    public static int NumberOfWorking = 1;
    public static int NumberOfCheck = 1;


    int graphHeight = 160 - 35 - 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},00);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_irp_page);
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        profile = (ImageButton) findViewById(R.id.irp_avatar);
        profile.setOnClickListener(this);
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        menu = (ImageButton) findViewById(R.id.irp_menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Main_page main_page = new Main_page();
                    main_page.NumPageEliment = 1;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    takeScreenshot();
//                    store(getScreenShot(rootView),"tmp_screenshot");
                    Intent intent = new Intent(irp_page.this, Main_page.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_sidein, R.anim.left_slideout);
                    finish();
                } catch (Exception e) {

                }
            }
        });

        // ########

        leftMarker = (LinearLayout) findViewById(R.id.leftMarker);
        centerMarker = (LinearLayout) findViewById(R.id.centerMarker);
        rightMarker = (LinearLayout) findViewById(R.id.rightMarker);
        LinearLayout columnBlue = (LinearLayout) findViewById(R.id.columnBlue);
        LinearLayout columnYellow = (LinearLayout) findViewById(R.id.columnYellow);
        LinearLayout columnRed = (LinearLayout) findViewById(R.id.columnRed);
        TextView leftText = (TextView) findViewById(R.id.textLeft);
        TextView centerText = (TextView) findViewById(R.id.textCenter);
        TextView rightText = (TextView) findViewById(R.id.textRight);
        Integer[] intArray = { NumberOfClosed, NumberOfCheck, NumberOfWorking };  // значения для диаграммы
        leftText.setText(String.valueOf(intArray[0]));
        centerText.setText(String.valueOf(intArray[1]));
        rightText.setText(String.valueOf(intArray[2]));

        int max = Collections.max(Arrays.asList(intArray));

        // высчитываем проценты
        int degParam1 = intArray[0] * 100 / max;
        int degParam2 = intArray[1] * 100 / max;
        int degParam3 = intArray[2] * 100 / max;

        final float scale = this.getResources().getDisplayMetrics().density;

        columnBlue.getLayoutParams().height = (int) ((graphHeight * degParam1 / 100) * scale + 0.5f);
        columnYellow.getLayoutParams().height = (int) ((graphHeight * degParam2 / 100) * scale + 0.5f);
        columnRed.getLayoutParams().height = (int) ((graphHeight * degParam3 / 100) * scale + 0.5f);
        columnBlue.requestLayout();
        ColumnClick(columnBlue, leftMarker);
        ColumnClick(columnYellow, centerMarker);
        ColumnClick(columnRed, rightMarker);



        /*
        BarChart barChart = findViewById(R.id.barChart);

        ArrayList<BarEntry> visitors = new ArrayList<>();
        visitors.add(new BarEntry(0f, 420));
        visitors.add(new BarEntry(1f, 475));
        visitors.add(new BarEntry(2f, 410));

        BarDataSet barDataSet = new BarDataSet(visitors, "visitors");


        // удаляем сетку
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getAxisLeft().setDrawGridLines(false);
        barChart.getAxisRight().setDrawGridLines(false);

        // убираем цифры от сетки слева и справа
        barChart.getAxisLeft().setEnabled(false);
        barChart.getAxisRight().setEnabled(false);

        // удаление внешней линии
        barChart.getAxisRight().setDrawAxisLine(false);
        barChart.getAxisLeft().setDrawAxisLine(false);
        barChart.getXAxis().setDrawAxisLine(false);

        // убираем описание
        barChart.setDescription(null);
        // убираем легенду
        barChart.getLegend().setEnabled(false);

        XAxis xAxis = barChart.getXAxis();
        // убираем цифры сверху
        xAxis.setEnabled(false);

        // Settings for Y-Axis
        YAxis leftAxis = barChart.getAxisLeft();
        YAxis rightAxis = barChart.getAxisRight();

        // устанавливаем минимальное значение 0
        leftAxis.setAxisMinValue(0f);
        rightAxis.setAxisMinValue(0f);


        // barChart.setRenderer(new MyRenderer(barChart, barChart.getAnimator(), barChart.getViewPortHandler()));


        BarData barData = new BarData(barDataSet);

        barData.setBarWidth(.5f);
        barChart.setData(barData);

        barDataSet.setColors(getResources().getColor(R.color.barChart_blue), getResources().getColor(R.color.barChart_yellow), getResources().getColor(R.color.barChart_red));
        barDataSet.setValueTextColor(Color.BLACK);

        barChart.invalidate(); */
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.irp_avatar:
                Profile profile = new Profile();
                Thread newThread = new Thread(() -> {
                    profile.backgroundWork();
                });
                newThread.start();
                try {
                    newThread.join();
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(this, Profile.class);
                    startActivity(intent);
                    finish();
                    break;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            default:
                break;
        }
    }


    private void ColumnClick(LinearLayout column, LinearLayout marker) {
        column.setOnClickListener(new View.OnClickListener() { //прослушиваем клик
            @Override
            public void onClick(View v) {
                try {
                    leftMarker.setVisibility(LinearLayout.INVISIBLE);
                    centerMarker.setVisibility(LinearLayout.INVISIBLE);
                    rightMarker.setVisibility(LinearLayout.INVISIBLE);
                    marker.setVisibility(LinearLayout.VISIBLE);

                } catch (Exception e) {

                }
            }
        });
    }


    public void showEvents(View view) {
        open_events open_events = new open_events();
        Thread newThread = new Thread(() -> {
            open_events.backgroundWork(view.getTag().toString());
        });

        newThread.start();
        try {
            newThread.join();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(this, open_events.class);
            startActivity(intent);
            finish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void showEventsDep(View view) {
        /*
        unit_events unit_events = new unit_events();
        Thread newThread = new Thread(() -> {
            unit_events.backgroundWork();
        });

        newThread.start();
        try {
            newThread.join();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(this, open_events.class);
            startActivity(intent);
            finish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/


        Intent intent = new Intent(this, unit_events.class);
        startActivity(intent);
        finish();
    }

//    public static Bitmap getScreenShot(View view) {
//        View screenView = view.getRootView();
//        screenView.setDrawingCacheEnabled(true);
//        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
//        screenView.setDrawingCacheEnabled(false);
//        return bitmap;
//    }
//
//    public static void store(Bitmap bm, String fileName){
//        final String  dirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
////                getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
//        System.out.println("EXTERNAL_PAСХ_ДИРЕКТОРИ = " + Environment.getExternalStorageDirectory().getAbsolutePath());
//        File dir = new File(dirPath);
//        if(!dir.exists())
//            dir.mkdirs();
//        File file = new File(dirPath, fileName);
//        try {
//            FileOutputStream fOut = new FileOutputStream(file);
//            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
//            fOut.flush();
//            fOut.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/pictures" + "/tmp_screen.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }



    public void showArchiv(View view) {
//        open_events open_events = new open_events();
//        Thread newThread = new Thread(() -> {
//            open_events.backgroundWork(view.getTag().toString());
//        });

//        newThread.start();
//        try {
//            newThread.join();
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

            Intent intent = new Intent(this, events_archive.class);
            startActivity(intent);
            finish();

//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    public void AllTask(View view) {
//        open_events open_events = new open_events();
//        Thread newThread = new Thread(() -> {
//            open_events.backgroundWork(view.getTag().toString());
//        });

//        newThread.start();
//        try {
//            newThread.join();
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

        Intent intent = new  Intent(this, unit_tasks.class);
        startActivity(intent);
        finish();

//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }
    public void MyTask(View view) {
//        open_events open_events = new open_events();
//        Thread newThread = new Thread(() -> {
//            open_events.backgroundWork(view.getTag().toString());
//        });

//        newThread.start();
//        try {
//            newThread.join();
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

        Intent intent = new  Intent(this, my_tasks.class);
        startActivity(intent);
        finish();

//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    public void TaskArchiv(View view) {
//        open_events open_events = new open_events();
//        Thread newThread = new Thread(() -> {
//            open_events.backgroundWork(view.getTag().toString());
//        });

//        newThread.start();
//        try {
//            newThread.join();
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

        Intent intent = new  Intent(this, tasks_archive.class);
        startActivity(intent);
        finish();

//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    public void StatIRp(View view) {
//        open_events open_events = new open_events();
//        Thread newThread = new Thread(() -> {
//            open_events.backgroundWork(view.getTag().toString());
//        });

//        newThread.start();
//        try {
//            newThread.join();
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

        Intent intent = new  Intent(this, stat_irp.class);
        startActivity(intent);
        finish();

//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

}


package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class events_archive extends AppCompatActivity {



    private ImageButton GoIrpArchiv;
    private ImageButton addEventsArchiv;
    private Button ShowButtons;
    private ImageButton ShowFiltres;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_open_events);
        TextView col3 = findViewById(R.id.col_3);
        MaterialButton showTickets = findViewById(R.id.ShowTickets);
        col3.setText("Дата завершения");
        showTickets.setText("Архив Событий");
        showTickets.setIcon(null);

        addEventsArchiv = findViewById(R.id.add_itemEVENTS);
        addEventsArchiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheetDialogAddEventsFromArchiv(v);
            }
        });



        ShowFiltres = findViewById(R.id.ButtonOpenFilterEvents);
        ShowFiltres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowFilterBot(v);
            }
        });

        GoIrpArchiv = findViewById(R.id.MenuIrpGo);
        GoIrpArchiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Main_page.NumPageEliment = 5;
                    takeScreenshot();
                    Intent intent = new Intent(events_archive.this, Main_page.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_sidein, R.anim.left_slideout);
                    finish();
                } catch (Exception e) {

                }
            }
        });

        // тестовые данные
        JSONObject obj = new JSONObject();

        try {
            obj.put("Id", "123");
            obj.put("Name", "Тест 3");
            obj.put("FQDN", "Fqdn.AgentUid1");
            obj.put("TagName", "my tag");
            obj.put("Status", "4");
            obj.put("Responsible", "Пупкин Виктор Александрович");
            obj.put("DateClose", "2021-06-29T15:33:22.411Z");

            createElement(obj);
            createElement(obj);
            createElement(obj);

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    public void ShowFilterBot(View view){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_events_archive);
    }

    public void showMore(View view) {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        LinearLayout parentLayout = (LinearLayout)view.getParent();
        LinearLayout openLayout = (LinearLayout)parentLayout.getChildAt(2);

        LinearLayout clickLayout = (LinearLayout)view;
        ImageView button = (ImageView)clickLayout.getChildAt(0);

        ViewGroup.LayoutParams params = openLayout.getLayoutParams();

        //openLayout.getVisibility() == View.GONE

        if (openLayout.getHeight() == 0) {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition()); // анимация
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            //openLayout.setVisibility(View.VISIBLE);
            button.setRotation(180);
            clickLayout.setBackgroundColor(Color.parseColor("#F7F5F5"));
        }

        else {
            //openLayout.setVisibility(View.GONE);

            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition());
            params.height = 0;
            button.setRotation(0);
            clickLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        openLayout.setLayoutParams(params);
    }


    // СОЗДАНИЕ НОВОГО ЭЛЕМЕНТА
    public void createElement(JSONObject event) throws JSONException {
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);


        String date = event.getString("DateClose");


        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2021-04-24T13:55:29.123Z", formatter);*/


        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);

        //--- LinearLayout parentLayout ---
        LinearLayout parentLayout = new LinearLayout(this);
        mainLayout.addView(parentLayout);
        createLinearLayout(parentLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout headerLayout ---
        LinearLayout headerLayout = new LinearLayout(this);
        parentLayout.addView(headerLayout);
        headerLayout.setOnClickListener(this::showMore);

        int dpAsPixels = inPixelFromDp(10);
        headerLayout.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);

        createLinearLayout(headerLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- ImageView arrowImage ---
        ImageView arrowImage = new ImageView(this);
        headerLayout.addView(arrowImage);

        dpAsPixels = inPixelFromDp(20);

        // устанавливаем ширину высоту
        ViewGroup.LayoutParams arrowParams = arrowImage.getLayoutParams();
        arrowParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;;
        arrowParams.width = dpAsPixels;
        arrowImage.setLayoutParams(arrowParams);

        arrowImage.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);


        //--- TextView idText ---
        TextView idText = new TextView(this);
        headerLayout.addView(idText);
        idText.setTypeface(open_sans);

        dpAsPixels = inPixelFromDp(100);

        createTextView(idText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, event.getString("Id"), "#4B4A4D",15);


        //--- LinearLayout nameLayout ---
        LinearLayout nameLayout = new LinearLayout(this);
        headerLayout.addView(nameLayout);
        dpAsPixels = inPixelFromDp(5);
        nameLayout.setPadding(dpAsPixels, 0, dpAsPixels, 0);

        createLinearLayout(nameLayout, 0, ViewGroup.LayoutParams.WRAP_CONTENT, 1, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- TextView nameText ---
        TextView nameText = new TextView(this);
        nameLayout.addView(nameText);
        nameText.setTypeface(open_sans);
        nameText.setMaxLines(1);
        nameText.setEllipsize(TextUtils.TruncateAt.END);
        nameText.setSingleLine(true);

        createTextView(nameText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, event.getString("Name"), "#2291EA", 15);


        //--- TextView dateText ---
        TextView dateText = new TextView(this);
        headerLayout.addView(dateText);
        dateText.setTypeface(open_sans);

        dpAsPixels = inPixelFromDp(120);
        createTextView(dateText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, getDate(date), "#4B4A4D",15);


        //--- LinearLayout lineLayout ---
        LinearLayout lineLayout = new LinearLayout(this);
        parentLayout.addView(lineLayout);
        lineLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- openLayout openLayout ---
        LinearLayout openLayout = new LinearLayout(this);
        parentLayout.addView(openLayout);

        createLinearLayout(openLayout, ViewGroup.LayoutParams.MATCH_PARENT, 0, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout showMoreLayout ---
        LinearLayout showMoreLayout = new LinearLayout(this);
        openLayout.addView(showMoreLayout);

        int dpAsPixelsH = inPixelFromDp(20);
        int dpAsPixelsV = inPixelFromDp(10);
        showMoreLayout.setPadding(dpAsPixelsH, dpAsPixelsV, dpAsPixelsH, dpAsPixelsV);


        createLinearLayout(showMoreLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);

        if (event.has("FQDN")) {
            createNewItem("Узел", event.getString("FQDN"), "#4B4A4D", showMoreLayout);
        }

        createNewItem("Тег", event.getString("TagName"), "#4B4A4D", showMoreLayout);

        if (event.getString("Status").equals("4")) {
            createNewItem("Статус", "Закрыт", "#FF5648", showMoreLayout);
        }

        // ??? не знаю есть ли такие
        if (event.getString("Status").equals("1")) {
            createNewItem("Статус", "В работе", "#479696", showMoreLayout);
        }

        if (event.getString("Status").equals("3")) {
            createNewItem("Статус", "Выполнен", "#FF5648", showMoreLayout);
        }

        createNewItem("Ответственный", event.getString("Responsible"), "#4B4A4D", showMoreLayout);


        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        dpAsPixelsV = inPixelFromDp(10);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER_HORIZONTAL, LinearLayout.VERTICAL);



        //--- LinearLayout lineOpenLayout ---
        LinearLayout lineOpenLayout = new LinearLayout(this);
        openLayout.addView(lineOpenLayout);
        lineOpenLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineOpenLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);

    }


    public void openBottomSheetDialogAddEventsFromArchiv(View view){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_newevents);
        bottomSheetDialog.show();
    }


        private void takeScreenshot() {
            Date now = new Date();
            android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

            try {
                // image naming and path  to include sd card  appending name you choose for file
                String mPath = Environment.getExternalStorageDirectory().toString() + "/pictures" + "/tmp_screen.jpg";

                // create bitmap screen capture
                View v1 = getWindow().getDecorView().getRootView();
                v1.setDrawingCacheEnabled(true);
                Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                v1.setDrawingCacheEnabled(false);

                File imageFile = new File(mPath);

                FileOutputStream outputStream = new FileOutputStream(imageFile);
                int quality = 100;
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                outputStream.flush();
                outputStream.close();


            } catch (Throwable e) {
                // Several error may come out with file handling or DOM
                e.printStackTrace();
            }
        }

    public void createNewItem(String title, String value, String valueColor, LinearLayout showMoreLayout) {
        Typeface raleway = ResourcesCompat.getFont(this, R.font.raleway_semibold_font);
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);
        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        int dpAsPixelsV = inPixelFromDp(10);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.LEFT, LinearLayout.VERTICAL);


        //--- TextView titleText ---
        TextView titleText = new TextView(this);
        elemLayout.addView(titleText);
        titleText.setTypeface(raleway);

        createTextView(titleText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 0, title, "#4B4A4D",15);


        //--- TextView valueText ---
        TextView valueText = new TextView(this);
        elemLayout.addView(valueText);
        valueText.setTypeface(open_sans);
        dpAsPixelsV = inPixelFromDp(6);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) valueText.getLayoutParams();
        params.setMargins(0, dpAsPixelsV, 0, 0);

        createTextView(valueText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 0, value, valueColor,15);
    }

    public void showNode(View view) {
        System.out.println(view.getTag());

        uzel_uzel uzel_uzel = new uzel_uzel();
        Thread newThread = new Thread(() -> {
            uzel_uzel.backgroundWork(view.getTag().toString());
        });

        newThread.start();
        try {
            newThread.join();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(this, uzel_uzel.class);
            startActivity(intent);
            //finish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public int inPixelFromDp(int sizeInDp) {
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp*scale + 0.5f);
        return dpAsPixels;
    }

    public void createTextView(TextView textView, int width, int height, int gravity, int weight, String text, String textColor, int textSize) {
        // устанавливаем ширину высоту и вес
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        textView.setLayoutParams(params);

        textView.setGravity(gravity);
        textView.setText(text);
        textView.setTextColor(Color.parseColor(textColor));
        textView.setTextSize(textSize);
    }

    public void createLinearLayout(LinearLayout linearLayout, int width, int height, int weight, int gravity, int orientation) {
        // устанавливаем ширину высоту и вес

        linearLayout.setOrientation(orientation);
        linearLayout.setGravity(gravity);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        linearLayout.setLayoutParams(params);
    }

    public String getDate(String dateString) {
        SimpleDateFormat formatBefore = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat formatAfter = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm:ss");

        Date date = null;
        String myDate = "";
        try {
            date = formatBefore.parse(dateString);
            myDate = formatAfter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return myDate;
    }
}
package com.example.isitoq_project.servercontroller;

//import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;

import com.example.isitoq_project.MainActivity;
        import com.example.isitoq_project.Profile;
import com.example.isitoq_project.irp_page;
import com.example.isitoq_project.open_events;
import com.example.isitoq_project.uzel_uzel;
import com.example.isitoq_project.infevents_main;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
        import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;


public class HttpController {
    private String Token;
    private static String AccessToken;
    private static String PhotoToken;

    public void PostLoginHttp(String stringLogin, String stringPassword) throws IOException {

//        Апишка на получения токена
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://192.168.2.241:10083/api/Token");
// Request parameters and other properties.
        List<NameValuePair> params = new ArrayList<NameValuePair>(2);
        params.add(new BasicNameValuePair("email", "v.pupkin@pisem.net")); //stringLogin
        params.add(new BasicNameValuePair("password", "D1sciptIstMacht")); //stringPassword
        httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
//Execute and get the response.
        HttpResponse response = httpclient.execute(httppost);
//         System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString + "KOk");
        GetToken(responseString);
        AccessToken = Token.split(": ")[1].replace("\"", "");

        // Profile
        ProfileHttp();
        // GetUserPhotoById(Profile.USerId);
        //MyTicketsHttp(1);
//        TicketHttp(112);
//        TicketHttp(108);
        //getNodeByUid("a4fb631b-d823-4b2f-87dc-f9fb6671b89f");
        GetUserPhotoToken();
//        AllUser();
//        StatisticsPeriodOfUsers();
//        StatisticsOfUsers(0);
//        StatisticsOfDepartments();
//        Departments();
//        GetTDO();


    }

    public void ProfileHttp() throws IOException {
//        Апишка для профиля
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://192.168.2.241:10083/api/Profile");
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
//Execute and get the response.
        HttpResponse response = httpclient.execute(httpget);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
        // System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
        try {
            GetProfileData(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void getNodeByUid(String uid) throws IOException {
//        Апишка для профиля
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://192.168.2.241:10083/api/Nodes/" + uid);
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
//Execute and get the response.
        HttpResponse response = httpclient.execute(httpget);
        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
        // System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
        System.out.println(responseString);

        try {
            JSONObject jsonObj = new JSONObject(responseString);

            uzel_uzel.NodeUid = jsonObj.getString("Uid");
            uzel_uzel.NodeName = jsonObj.getString("Name");
            uzel_uzel.NodeDescription = jsonObj.getString("Description");
            uzel_uzel.NodeDateCreation = jsonObj.getString("DateCreation");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void GetProfileData(String _RequestData) throws JSONException {
        JSONObject jsonObj = new JSONObject(_RequestData);
        String userId = jsonObj.getString("Id");
        String userName = jsonObj.getString("Name");
        String userMail = jsonObj.getString("Email");
        String userPhone = jsonObj.getString("Phone");
        String department = jsonObj.getJSONObject("Department").getString("Name");
        Profile.USerId = userId;
        Profile.USerName = userName;
        Profile.USerMail = userMail;
        Profile.USerPhone = userPhone;
        Profile.Department = department;
        System.out.println( userName + " Проверка " + userMail + " Проверка " + userPhone + "  Проверка " + department);
        System.out.println(userId);

    }


    public void PostTicketHttp(String cardF, String cardCritp,String cardFQDN,
                               String cardTeg1,String cardDep1,String cardOrigin1,String cardDeadLine,String cardDM,
                               String cardDic,String cardDangenMMen,String cardAddsis,String CardCow,String cardMone,
                               String cardIp,String cardBedBoy,String cardFQDNUzel,String cardDateOpenTotot, String CardTimeOpenDo) throws IOException {
//        Апишка для профиля
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://192.168.2.241:83/api/Tickets");
// Request parameters and other properties.
        List<NameValuePair> params = new ArrayList<NameValuePair>(2);
        params.add(new BasicNameValuePair("name", cardF)); //stringLogin
        params.add(new BasicNameValuePair("criticality", cardCritp)); //stringPassword
        params.add(new BasicNameValuePair("tagId", cardTeg1)); //stringPassword
        params.add(new BasicNameValuePair("departmentId", cardDep1)); //stringPassword
        params.add(new BasicNameValuePair("sourceId", cardDM)); //stringPassword
        params.add(new BasicNameValuePair("dateLimit", cardDeadLine)); //stringPassword
        params.add(new BasicNameValuePair("canonicalNodeUid", cardFQDN)); //stringPassword
        params.add(new BasicNameValuePair("source", cardOrigin1)); //stringPassword
        params.add(new BasicNameValuePair("description", cardDic)); //stringPassword
        params.add(new BasicNameValuePair("relatedUsers", cardDangenMMen)); //stringPassword
        params.add(new BasicNameValuePair("dateLimit", cardDeadLine)); //stringPassword
        params.add(new BasicNameValuePair("RegardSystem", cardAddsis)); //stringPassword
        params.add(new BasicNameValuePair("attackersUserName", cardBedBoy)); //stringPassword
        params.add(new BasicNameValuePair("attackersIP", cardIp)); //stringPassword
        params.add(new BasicNameValuePair("attackersFQDN", cardFQDNUzel)); //stringPassword
        params.add(new BasicNameValuePair("dateIncident", cardDateOpenTotot + CardTimeOpenDo)); //stringPassword

        httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
//Execute and get the response.
        HttpResponse response = httpclient.execute(httppost);
//         System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
        try {
            GetTicketById(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void TicketHttp(String ticketId) throws IOException {
//        Апишка для профиля
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://192.168.2.241:10083/api/Tickets/" + ticketId);
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
//Execute and get the response.
        HttpResponse response = httpclient.execute(httpget);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
        // System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
        // System.out.println(responseString);

        try {
            GetTicketById(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void GetTicketById(String _RequestData) throws JSONException {
        JSONObject jsonObj = new JSONObject(_RequestData);
        String status = jsonObj.getString("Status");
        infevents_main.DateOpen = jsonObj.getString("DateOpen");  // дата создания
        infevents_main.DateLimit = jsonObj.getString("DateLimit"); // завершить до
        if (jsonObj.has("DateIncident")) infevents_main.DateIncident = jsonObj.getString("DateIncident"); // дата регистрации события
        infevents_main.Name = jsonObj.getString("Name"); // название
        infevents_main.Criticality = jsonObj.getString("Criticality"); // уровень критичности (1 - средний, 2 - высокий)
        if (jsonObj.has("Source")) infevents_main.Source = jsonObj.getJSONObject("Source").getString("Name"); // источник
        infevents_main.Department = jsonObj.getJSONObject("Department").getString("Name");
        infevents_main.ResponsibleUser = jsonObj.getJSONObject("User").getString("Name");

        if (jsonObj.has("Node")) infevents_main.Node = jsonObj.getJSONObject("Node").getString("FQDN");
        if (jsonObj.has("RegardSystem")) infevents_main.RegardSystem = jsonObj.getJSONObject("RegardSystem").getString("Name");
        if (jsonObj.has("LostExpectancy")) infevents_main.LostExpectancy = jsonObj.getString("LostExpectancy");
        if (jsonObj.has("LostActual")) infevents_main.LostActual = jsonObj.getString("LostActual");
        if (jsonObj.has("AttackersIP")) infevents_main.AttackersIP = jsonObj.getString("AttackersIP");
        if (jsonObj.has("AttackersUserName")) infevents_main.AttackersUserName = jsonObj.getString("AttackersUserName");
        if (jsonObj.has("AttackersFQDN")) infevents_main.AttackersFQDN = jsonObj.getString("AttackersFQDN");
        if (jsonObj.has("Description")) infevents_main.Description = jsonObj.getString("Description");

        JSONArray relatedUsers = jsonObj.getJSONArray("RelatedUsers");
        String[] processingRoleArr = new String[relatedUsers.length()];
        String[] userNameArr = new String [relatedUsers.length()];
        String[] userIdArr = new String[relatedUsers.length()];
        for (int i = 0; i < relatedUsers.length(); i++)
        {
            processingRoleArr[i] = relatedUsers.getJSONObject(i).getString("ProcessingRole"); // 0 - наблюдатель, 2 - ответственный, 3 - контролирующий
            userNameArr[i] = relatedUsers.getJSONObject(i).getJSONObject("User").getString("Name");
            userIdArr[i] = relatedUsers.getJSONObject(i).getString("UserId");
        }
        int indexResponsible = Arrays.asList(processingRoleArr).indexOf("2");
        String responsible = userNameArr[indexResponsible];  // ответственный
        /*System.out.println(status + " Проверка " + dateOpen + " Проверка " + dateLimit + " Проверка " + name + " Проверка " + criticality + " Проверка " + responsible + " Проверка " + source + " Проверка " + FQDN + " Проверка " + department);
        System.out.println(Arrays.toString(processingRoleArr));
        System.out.println(Arrays.toString(userNameArr));
        System.out.println(Arrays.toString(userIdArr));*/
    }

    public void MyTicketsHttp(String num) throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        System.out.println("NUM");
        System.out.println(num);

        String Url = "http://192.168.2.241:10083/api/Tickets?tab=" + num + "&sortField=1&page=1&results=30";

        HttpGet httpget = new HttpGet(Url);
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
//Execute and get the response.
//      Апишка для получения информации "мои события"
        HttpResponse response = httpclient.execute(httpget);
        System.out.println("response");
        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
        //System.out.println("entity");
        //System.out.println(entity);
        String responseString = "";
        if (!response.getStatusLine().toString().contains("404")) responseString = new BasicResponseHandler().handleResponse(response);
        //System.out.println("responseString");
        //System.out.println(responseString);
        JSONArray ArrayjsonObj;
        try {

            if (response.getStatusLine().toString().contains("404")) ArrayjsonObj = new JSONArray();
            else ArrayjsonObj = new JSONArray(responseString);

            open_events.eventsArray = ArrayjsonObj;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void GetTicketsData(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        String nod = "";
        String nodUid = "";
        String nodFQDN = "";

        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            String ticketsTag = c.getString("Tag");
            String ticketsId = c.getString("Id");
            String ticketsName = c.getString("Name");
            String ticketsStatus = c.getString("Status");
            String ticketsDatalimit = c.getString("DateLimit");
            String ticketsTagId = c.getString("TagId");
            String ticketsNumberOfTasks = c.getString("NumberOfTasks");
            String ticketsCanonicalNodeUid = "";
            try{
                 ticketsCanonicalNodeUid = c.getString("CanonicalNodeUid");
            }
            catch (JSONException e) {
                ticketsCanonicalNodeUid = "";
            }
            String ticketsDepartmentId = c.getString("DepartmentId");
            String ticketsSourceId = c.getString("SourceId");
            String ticketsDataOpen = c.getString("DateOpen");
            JSONObject c2 = new JSONObject(ticketsTag);
            String TagTickets = c2.getString("Name");
            try {
                String ticketsNode = c.getString("Node");
                JSONObject c1 = new JSONObject(ticketsNode);
                String NodeTickets = c1.getString("Name");
                String UidTickets = c1.getString("Uid");
                String FQDNTickets = c1.getString("FQDN");
                nod = NodeTickets;

            } catch (JSONException e) {
                nod = "";
            }
            System.out.println(ticketsId + " Проверка " + ticketsName + " Проверка " + ticketsStatus + "  Проверка " + ticketsDatalimit + "  Проверка " + ticketsDataOpen + " Проверка " + nod + " Проверка " + TagTickets);

        }
    }

    public void Counttab0(int num) throws IOException {
//        Апишка для статистики
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://192.168.2.241:83/api/Tickets/Count?tab=" + num);
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        String responseString = new BasicResponseHandler().handleResponse(response);
        try {
            GetCounttab0(responseString, num);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void GetCounttab0(String _RequestData, int num) throws JSONException {
        JSONObject jsonObj = new JSONObject(_RequestData);
        String RelatedId = jsonObj.getString("Count");
            if(num == 0){
//                irp_page.NumberOfClosed= Integer.parseInt(RelatedId);
                irp_page.NumberOfClosed= 6;
                System.out.println(Integer.parseInt(RelatedId));
            }
            else if(num == 1){
                irp_page.NumberOfWorking= Integer.parseInt(RelatedId);
                System.out.println(Integer.parseInt(RelatedId));
            }
            else if(num == 2){
                irp_page.NumberOfCheck = Integer.parseInt(RelatedId);
                System.out.println(Integer.parseInt(RelatedId));
            }


//            irp_page.NumberOfCheck = Integer.parseInt(NumberOfCheck);
//            irp_page.NumberOfWorking= Integer.parseInt(NumberOfWorking);
//            irp_page.NumberOfClosed= Integer.parseInt(RelatedId);

        }


    public void StatisticsOfUsers(int num) throws IOException {
//        Апишка для статистики
        HttpClient httpclient = new DefaultHttpClient();
        String Url = "";
        if(num == 0){
            Url = "http://192.168.2.241:10083/api/Tickets/StatisticsOfUsers";
        }
        else if (num == 1){
            Url = "http://192.168.2.241:10083/api/Tickets/StatisticsOfUsers?showChild=true";
        }
        else if (num == 2){
            Url = "http://192.168.2.241:10083/api/Tickets/StatisticsOfUsers?showChild=false";
        }
        HttpGet httpget = new HttpGet(Url);
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
//Execute and get the response.
        HttpResponse response = httpclient.execute(httpget);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
        try {
            GetStatisticsUsers(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void StatisticsPeriodOfUsers(int num, int date_start, int data_end) throws IOException {
//        Апишка для статистики
        HttpClient httpclient = new DefaultHttpClient();
        String Url = "";
        if(num == 0){
            Url = "http://192.168.2.241:10083/api/Tickets/StatisticsPeriodOfUsers?date_start="+date_start+"&date_end="+data_end+"&ShowChild=false";
        }
        else if (num == 1){
            Url = "http://192.168.2.241:10083/api/Tickets/StatisticsPeriodOfUsers?date_start="+date_start+"&date_end="+data_end+"&ShowChild=true";
        }
        HttpGet httpget = new HttpGet(Url);
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
//Execute and get the response.
        HttpResponse response = httpclient.execute(httpget);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
        try {
            GetStatisticsUsers(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void GetStatisticsUsers(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            String RelatedId = c.getString("RelatedId");
            String RelatedName = c.getString("RelatedName");
            String ParentDepartmentId = c.getString("ParentDepartmentId");
            String RelatedSource = c.getString("RelatedSource");
            String NumberOfOpened = c.getString("NumberOfOpened");
            String NumberOfWorking = c.getString("NumberOfWorking");
            String NumberOfReWork = c.getString("NumberOfReWork");
            String NumberOfExpired = c.getString("NumberOfExpired");
            String NumberOfCheck = c.getString("NumberOfCheck");
            String NumberOfClosed = c.getString("NumberOfClosed");
            irp_page.NumberOfCheck = Integer.parseInt(NumberOfCheck);
            irp_page.NumberOfWorking= Integer.parseInt(NumberOfWorking);
            irp_page.NumberOfClosed= Integer.parseInt(NumberOfClosed);
            System.out.println(NumberOfWorking +"кок"+ NumberOfCheck +"лщщл" + NumberOfClosed);

        }
    }

    public void StatisticsOfDepartments() throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://192.168.2.241:10083/api/Tickets/StatisticsOfDepartments");
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        HttpResponse response = httpclient.execute(httpGet);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
                try {
                    GetStatisticsOfDepartments(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void GetStatisticsOfDepartments(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            String RelatedId = c.getString("RelatedId");
            String RelatedName = c.getString("RelatedName");
            String ParentDepartmentId = c.getString("ParentDepartmentId");
            String RelatedSource = c.getString("RelatedSource");
            String NumberOfOpened = c.getString("NumberOfOpened");
            String NumberOfWorking = c.getString("NumberOfWorking");
            String NumberOfReWork = c.getString("NumberOfReWork");
            String NumberOfExpired = c.getString("NumberOfExpired");
            String NumberOfCheck = c.getString("NumberOfCheck");
            String NumberOfClosed = c.getString("NumberOfClosed");
        }

    }

    public void GetUserPhotoToken() throws IOException {
//        System.out.println("Koook");
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://192.168.2.241:83/api/photoToken");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        HttpResponse response = httpclient.execute(httpPost);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
        PhotoGetToken(responseString);
//        GetUserPhotoById(Profile.USerId);
    }

    public void PhotoGetToken(String _RequestToken)
    {
        try {
            StringTokenizer getToken = new StringTokenizer(_RequestToken,",{}");
            while (getToken.hasMoreElements()){
                String KeyToken = getToken.nextToken();
                String KeyData = getToken.nextToken();
//              System.out.println(KeyToken);
                PhotoToken = KeyToken;
                PhotoToken = KeyToken.split(": ")[1].replace("\"", "");
               System.out.println(PhotoToken);
                GetUserPhotoById(Profile.USerId, PhotoToken);

            }
        }
        catch (Exception a){
        }
    }
    public void GetUserPhotoById(String id, String Token) throws IOException {
        System.out.println(id);
//        HttpClient httpclient = new DefaultHttpClient();
//        HttpGet httpget = new HttpGet("http://192.168.2.241:10083/api/Profile/avatar/" + id +"?token="+ Token);
//        URL url = new URL("http://192.168.2.241:10083/api/Profile/avatar/" + id +"?token="+ Token);

//        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
////        System.out.println(connection);
//        connection.setDoInput(true);
//        connection.connect();
////        System.out.println(connection);
//        InputStream inputStream = connection.getInputStream();
//        Bitmap Photo = BitmapFactory.decodeStream(inputStream);
//        System.out.println(Photo);
        Profile.PhotoPop = "http://192.168.2.241:10083/api/Profile/avatar/" + id +"?token="+ Token;

//        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
////Execute and get the response.
//        HttpResponse response = httpclient.execute(httpget);
////        System.out.println(response.getStatusLine());
//        HttpEntity entity = response.getEntity();
////        System.out.println(entity);
//        String responseString = new BasicResponseHandler().handleResponse(response);
////        System.out.println(responseString);
//        Profile.PhotoPop = responseString;

    }

//////////////////////////////////////////////////////////

    public void Departments(int num) throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://192.168.2.241:10083/api/departments");
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        HttpResponse response = httpclient.execute(httpGet);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
        try {
            if(num == 1) {
                GetDepartmentsFromFilters(responseString);
            }
            else if (num == 2) {
                GetDepartments(responseString);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void GetDepartments(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        String nod = "";
        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            String DepartmentsName = c.getString("Name");
            String DepartmentsisDefault = c.getString("isDefault");
            String DepartmentsIsActive = c.getString("IsActive");
            String DepartmentsLeaderId = c.getString("LeaderId");
            String DepartmentsLeader = c.getString("Leader");
            JSONObject c2 = new JSONObject(DepartmentsLeader);
            String LiderName = c2.getString("Name");
            String LiderEmail = c2.getString("Email");
            String LiderIsActive = c2.getString("IsActive");
            String LiderCanDelegatePermissions = c2.getString("CanDelegatePermissions");
            String LiderDepartmentId = c2.getString("DepartmentId");
            String Departmentsdepo = c.getString("Departments");
            JSONArray c3D = new JSONArray(Departmentsdepo);
            for (int j = 0; j < c3D.length(); j++){
                JSONObject c3 =  c3D.getJSONObject(j);
                String DepartmentsdepoName = c3.getString("Name");
                String DepartmentsdepoisDefault = c3.getString("isDefault");
                String DepartmentsdepoLeaderId = c3.getString("LeaderId");
                String DepartmentsdepoIsActive = c3.getString("IsActive");
                String DepartmentsdepoId = c3.getString("Id");
                String DepartmentsdepoParentDepartmentId = c3.getString("ParentDepartmentId");
                String DepartmentsdepoLeader = c3.getString("Leader");
                JSONObject c4 = new JSONObject(DepartmentsdepoLeader);
                String DepartmentsdepoLeaderName = c4.getString("Name");
                String DepartmentsdepoLeaderEmail = c4.getString("Email");
                String DepartmentsdepoLeaderIsActive = c4.getString("IsActive");
                String DepartmentsdepoLeaderCanDelegatePermissions = c4.getString("CanDelegatePermissions");
                String DepartmentsdepoLeaderDepartmentId = c4.getString("DepartmentId");
                System.out.println( " 1 Проверка " + DepartmentsdepoName + " Проверка " + DepartmentsdepoisDefault+ " Проверка " + DepartmentsdepoParentDepartmentId+
                        " Проверка " + DepartmentsdepoLeaderId+ " Проверка " + DepartmentsdepoIsActive+ " Проверка " + DepartmentsdepoId+
                        " Проверка "+ " Проверка " + DepartmentsdepoLeaderName+ " Проверка " + DepartmentsdepoLeaderEmail+
                        " Проверка " + DepartmentsdepoLeaderIsActive+ " Проверка " + DepartmentsdepoLeaderCanDelegatePermissions+ " Проверка " + DepartmentsdepoLeaderDepartmentId
                );
            }


            System.out.println(DepartmentsName + " 2 Проверка " + DepartmentsisDefault + " Проверка " + DepartmentsIsActive +
                    "  Проверка " + DepartmentsLeaderId + "  Проверка " +
                    " Проверка " + LiderIsActive + " Проверка " + LiderName+ " Проверка " + LiderEmail+
                    " Проверка " + LiderCanDelegatePermissions+ " Проверка " + LiderDepartmentId+ " Проверка"
            );

        }
    }

    public void GetDepartmentsFromFilters(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        List<String> list = new ArrayList<String>();
        String[] stockArr = new String[list.size()];
        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            list.add(c.getString("Name"));
//            String DepartmentsName = c.getString("Name");
            String DepartmentsLeader = c.getString("Leader");
//            JSONObject c2 = new JSONObject(DepartmentsLeader);
//            String LiderName = c2.getString("Name");
//            String LiderDepartmentId = c2.getString("DepartmentId");
//            String Departmentsdepo = c.getString("Departments");
//            JSONArray c3D = new JSONArray(Departmentsdepo);
//            for (int j = 0; j < c3D.length(); j++){
//                JSONObject c3 =  c3D.getJSONObject(j);
//                list.add(c.getString("Name"));
////                String DepartmentsdepoName = c3.getString("Name");
//                String DepartmentsdepoLeaderId = c3.getString("LeaderId");
//                String DepartmentsdepoId = c3.getString("Id");
//                String DepartmentsdepoLeader = c3.getString("Leader");
//                JSONObject c4 = new JSONObject(DepartmentsdepoLeader);
//                list.add(c.getString("Name"));
//                String DepartmentsdepoLeaderName = c4.getString("Name");
//                String DepartmentsdepoLeaderDepartmentId = c4.getString("DepartmentId");
//            }
        }
        stockArr = list.toArray(stockArr);
        open_events.data2 = stockArr;
//        System.out.println(Arrays.toString(stockArr));


    }



////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////
    //Фильтры
  public void AllTick() throws IOException {
//        Апишка для статистики
      HttpClient httpclient = new DefaultHttpClient();
      HttpGet httpget = new HttpGet("http://192.168.2.241:10083/api/TicketTags/All");
      httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
      System.out.println("Токен: " + AccessToken);
//Execute and get the response.
      HttpResponse response = httpclient.execute(httpget);
//        System.out.println(response.getStatusLine());
      HttpEntity entity = response.getEntity();
//        System.out.println(entity);
      String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
      try {
          GetAllTick(responseString);
      } catch (JSONException e) {
          e.printStackTrace();
      }
  }
    public void GetAllTick(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        List<String> list = new ArrayList<String>();
        String[] stockArr = new String[list.size()];
        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            list.add(c.getString("Name"));
//            String[] kok = AllName.split(",");
//            open_events.data1 = AllName.split(",");
        }
//        System.out.println(list);
        stockArr = list.toArray(stockArr);

//        open_events.data1 = stockArr;
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    public void AllUser() throws IOException {
//        Апишка для статистики
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://192.168.2.241:10083/api/Users/all");
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
//Execute and get the response.
        HttpResponse response = httpclient.execute(httpget);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
        try {
            GetAllUser(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void GetAllUser(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        List<String> list = new ArrayList<String>();
        String[] stockArr = new String[list.size()];
        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            list.add(c.getString("Name"));
//            String[] kok = AllName.split(",");
//            open_events.data1 = AllName.split(",");
        }
//        System.out.println(list);
        stockArr = list.toArray(stockArr);

        open_events.AllUser = stockArr;
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void FilterTikc(int num) throws IOException {
//        Апишка для статистики
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://192.168.2.241:83/api/Tickets/AvalibleTags?tab=" + num);
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
//Execute and get the response.
        HttpResponse response = httpclient.execute(httpget);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
        try {
            GetFilterTikc(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void GetFilterTikc(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        List<String> list = new ArrayList<String>();
        String[] stockArr = new String[list.size()];
        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            list.add(c.getString("Name"));
//            String[] kok = AllName.split(",");
//            open_events.data1 = AllName.split(",");
        }
//        System.out.println(list);
        stockArr = list.toArray(stockArr);

        open_events.dataTag = stockArr;
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public void ChildsForLeader() throws IOException {
//        Апишка для статистики
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://192.168.2.241:83/api/departments/ChildsForLeader");
        httpget.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + AccessToken);
        System.out.println("Токен: " + AccessToken);
//Execute and get the response.
        HttpResponse response = httpclient.execute(httpget);
//        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
//        System.out.println(entity);
        String responseString = new BasicResponseHandler().handleResponse(response);
//        System.out.println(responseString);
        try {
            GetChildsForLeader(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void GetChildsForLeader(String _RequestData) throws JSONException {
        JSONArray ArrayjsonObj = new JSONArray(_RequestData);
        List<String> list = new ArrayList<String>();
        String[] stockArr = new String[list.size()];
        for (int i = 0; i < ArrayjsonObj.length(); i++) {
            JSONObject c = ArrayjsonObj.getJSONObject(i);
            list.add(c.getString("Name"));
//            String[] kok = AllName.split(",");
//            open_events.data1 = AllName.split(",");
        }
//        System.out.println(list);
        stockArr = list.toArray(stockArr);

        open_events.datadep = stockArr;
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }



/////////////////////////////////////////////////////////////////////////////////

    public void GetToken(String _RequestToken)
    {
        try {
            StringTokenizer getToken = new StringTokenizer(_RequestToken,",{}");
            while (getToken.hasMoreElements()){
                String KeyToken = getToken.nextToken();
                String KeyData = getToken.nextToken();
                Token = KeyToken;
                AccessToken = Token.split(": ")[1].replace("\"", "");
                Counttab0(0);
                Counttab0(1);
                Counttab0(2);
////               System.out.println(KeyToken);
////               System.out.println(KeyData);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                OpenView();
            }
        }
        catch (Exception a){

        }

    }
    public void OpenView()
    {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Token);
        String Suc ="access_token";
        if(Token.lastIndexOf(Suc)!=-1){
            System.out.println("OK");
            MainActivity.Start();
        }
        else{
            System.out.println("Нет совпадений");
        }

    }


}
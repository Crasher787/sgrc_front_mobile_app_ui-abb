package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.isitoq_project.servercontroller.HttpController;

import java.io.IOException;
import java.net.URI;

public class Main_page extends AppCompatActivity {

    TextView irp;
    TextView GotoProf;
    RelativeLayout irp_item;
    ImageView mini_screen;
    public static int NumPageEliment = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        irp = (TextView) findViewById(R.id.textView1);
        irp_item = (RelativeLayout) findViewById(R.id.irp_link_item);
        GotoProf = (TextView) findViewById(R.id.goToProfile);

        mini_screen = (ImageView) findViewById(R.id.tmp_mini_screen);
        mini_screen.setImageURI(Uri.parse("file:///storage/emulated/0/pictures/tmp_screen.jpg"));
//        mini_screen.setImageResource(R.drawable.whitee);

        GotoProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    Intent intent = new Intent(Main_page.this, Profile.class);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
//                    finish();
//                } catch (Exception e) {
//                }

                Profile profile = new Profile();
                Thread newThread = new Thread(() -> {
                    profile.backgroundWork();
                });
                newThread.start();
                try {
                    newThread.join();
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(Main_page.this, Profile.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        irp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Thread newThread = new Thread(() -> {
                        backgroundWork();
                    });
                    newThread.start();
                    try {
                        newThread.join();
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(Main_page.this, irp_page.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

//                    Intent intent = new Intent(Main_page.this, irp_page.class);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
//                    finish();
                } catch (Exception e) {

                }
            }
        });

        irp_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    System.out.println(NumPageEliment);
                    if(NumPageEliment == 1){
                        Intent intent = new Intent(Main_page.this, irp_page.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 2){
                        Intent intent = new Intent(Main_page.this, Profile.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 3){
                        Intent intent = new Intent(Main_page.this, open_events.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 4){
                        Intent intent = new Intent(Main_page.this, unit_events.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 5){
                        Intent intent = new Intent(Main_page.this, events_archive.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 6){
                        Intent intent = new Intent(Main_page.this, unit_tasks.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 7){
                        Intent intent = new Intent(Main_page.this, my_tasks.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 8){
                        Intent intent = new Intent(Main_page.this, tasks_archive.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 9){
                        Intent intent = new Intent(Main_page.this, stat_irp.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 10){
                        Intent intent = new Intent(Main_page.this, infevents_main.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }
                    else if(NumPageEliment == 11){
                        Intent intent = new Intent(Main_page.this, uzel_uzel.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_sidein, R.anim.right_slideout);
                        finish();
                    }

                } catch (Exception e) {

                }
            }
        });

    }

    public void backgroundWork() {
        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    HttpController httpController = new HttpController();
                    httpController.Counttab0(0);
                    httpController.Counttab0(1);
                    httpController.Counttab0(2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

}
package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class unit_events extends AppCompatActivity {


    private ImageButton GoIrpUnits;
    private ImageButton addEventsUnits;
    private ImageButton OpenFiltertow;
    private AutoCompleteTextView cardSort;
    private AutoCompleteTextView cardDep;
    private AutoCompleteTextView cardStatus;
    private LinearLayout StatusLiner;
    private LinearLayout TegLiner;
    private ImageButton OpenFilter;
    private AutoCompleteTextView cardTeg;
    private AutoCompleteTextView cardOring;
    private AutoCompleteTextView cardFIO;
    private MultiAutoCompleteTextView cardRole;;
    private AutoCompleteTextView cardNameEvent;
    private AutoCompleteTextView cardFQDN;
    private AutoCompleteTextView cardDateOpenTo;
    private AutoCompleteTextView cardDateOpenDo;
    private AutoCompleteTextView cardDateCloseTo;
    private AutoCompleteTextView cardDateCloseDo;
    public int NumIf = 0;
    public static String[] dataRole = {"Наблюдатель","Соиспольнитель","Ответственный","Контролирующий"};
    public static String[] data2 = {"Test"};
    //    public static String[] dataRole = {"Наблюдатель","Соиспольнитель","Ответственный","Контролирующий"};
    public static String[] dataSort = {"Сначала срочные","По порядку ID","По названиям(А-Я) ","По времени"};
    public static String[] datadep = {"Test"};
    public static String[] dataStatus = {"Открыт","В работе","Возвращён в работу","Выполнен","Закрыт","Отклонён"};
    public static String[] dataTag = {"Test"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_open_events);

        TextView col3 = findViewById(R.id.col_3);
        col3.setText("Тег");
        MaterialButton showTickets = findViewById(R.id.ShowTickets);
        showTickets.setText("События подразделения");
        showTickets.setIcon(null);

        OpenFilter = findViewById(R.id.ButtonOpenFilterEvents);
        OpenFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheetDialog(v);
            }

        });

        addEventsUnits = findViewById(R.id.add_itemEVENTS);
        addEventsUnits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomSheetDialogAddEventsFromunits(v);
            }
        });
        OpenFiltertow = findViewById(R.id.ButtonOpenFilterEvents);
        OpenFiltertow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBottomShee(v);
            }
        });
        GoIrpUnits = findViewById(R.id.MenuIrpGo);
        GoIrpUnits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Main_page.NumPageEliment = 4;
                    takeScreenshot();
                    Intent intent = new Intent(unit_events.this, Main_page.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_sidein, R.anim.left_slideout);
                    finish();
                } catch (Exception e) {

                }
            }
        });



        // тестовые данные
        JSONObject obj = new JSONObject();

        try {
            obj.put("Id", "123");
            obj.put("Name", "Test");
            obj.put("Node", "Node2 name");
            obj.put("Tag", "my tag");
            obj.put("Status", "1");

            createElement(obj);
            createElement(obj);
            createElement(obj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void openBottomShee(View view) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_unit_events);
    }


    public void openBottomSheetDialog(View view){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_open_events);
        TegLiner = bottomSheetDialog.findViewById(R.id.TegLinerTow);
        StatusLiner = bottomSheetDialog.findViewById(R.id.StatusLinerTow);

        bottomSheetDialog.findViewById(R.id.ExitButtonFilterOpenEve).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });


        cardSort = bottomSheetDialog.findViewById(R.id.CardSort);
        cardDep = bottomSheetDialog.findViewById(R.id.CardDep);
        cardStatus = bottomSheetDialog.findViewById(R.id.CardStatus);
        cardTeg = bottomSheetDialog.findViewById(R.id.CardTeg);
        cardOring = bottomSheetDialog.findViewById(R.id.CardOring);
        cardFIO = bottomSheetDialog.findViewById(R.id.CardFIO);
        cardRole = bottomSheetDialog.findViewById(R.id.CardRole);
        cardNameEvent = bottomSheetDialog.findViewById(R.id.CardNameEvent);
        cardFQDN = bottomSheetDialog.findViewById(R.id.CardFQDN);
        cardDateOpenTo = bottomSheetDialog.findViewById(R.id.CardDateOpenTo);
        cardDateOpenDo = bottomSheetDialog.findViewById(R.id.CardDateOpenDo);
        cardDateCloseTo = bottomSheetDialog.findViewById(R.id.CardDateCloseTo);
        cardDateCloseDo = bottomSheetDialog.findViewById(R.id.CardDateCloseDo);
        ArrayAdapter adapter1 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataSort);
        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, datadep);
        ArrayAdapter adapter3 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataStatus);
        ArrayAdapter adapter4 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataTag);
        ArrayAdapter adapterRole = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataRole);

//        CreteButtonFitr(linerPop,"pop");
        cardSort.setAdapter(adapter1);
//        cardSort.setText(cardSort.getAdapter().getItem(2).toString(), false);
        cardSort.setThreshold(1);
        cardDep.setAdapter(adapter2);
        cardDep.setThreshold(1);
        cardStatus.setAdapter(adapter3);
        cardStatus.setThreshold(1);

        LinearLayout linerPop =  bottomSheetDialog.findViewById(R.id.LinerPop);
        cardStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = String.valueOf(parent.getItemAtPosition(position));
//                CreteButtonFitr(StatusLiner, text);

            }
        });


//        cardTeg.setAdapter(adapter4);
//        cardTeg.setThreshold(1);
        cardTeg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = String.valueOf(parent.getItemAtPosition(position));
//                CreteButtonFitr(TegLiner, text);
            }
        });


        cardRole.setAdapter(adapterRole);
        cardRole.setThreshold(1);
        cardRole.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

//        bottomSheetDialog.findViewById(R.id.CardRole).setOnItemClickListener(new AdapterView.OnItemClickListener(){
//
//            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//                String text = (String) parent.getItemAtPosition(position);
//                CreteButtonFitr(linerPop,text);
//            }
//        });



        bottomSheetDialog.findViewById(R.id.CardDateOpenTo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumIf = 1;
                DialogFragment dataPiker = new date_pick();
                dataPiker.show(getSupportFragmentManager(), "DataPiker");
            }
        });
        bottomSheetDialog.findViewById(R.id.CardDateOpenDo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumIf = 2;
                DialogFragment dataPiker = new date_pick();
                dataPiker.show(getSupportFragmentManager(), "DataPiker");
            }
        });

        bottomSheetDialog.findViewById(R.id.CardDateCloseTo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumIf = 3;
                DialogFragment dataPiker = new date_pick();
                dataPiker.show(getSupportFragmentManager(), "DataPiker");
            }
        });

        bottomSheetDialog.findViewById(R.id.CardDateCloseDo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumIf = 4;
                DialogFragment dataPiker = new date_pick();
                dataPiker.show(getSupportFragmentManager(), "DataPiker");
            }
        });


        bottomSheetDialog.findViewById(R.id.ClearFilterOpenEve).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardSort.setText(null);
                cardDep.setText(null);
                cardStatus.setText(null);
                cardTeg.setText(null);
                cardOring.setText(null);
                cardFIO.setText(null);
                cardRole.setText(null);
                cardNameEvent.setText(null);
                cardFQDN.setText(null);
                cardDateOpenTo.setText(null);
                cardDateOpenDo.setText(null);
                cardDateCloseTo.setText(null);
                cardDateCloseDo.setText(null);
            }
        });
        bottomSheetDialog.show();
    }

    public void CreteButtonFitr(LinearLayout liner, String text)
    {
        MaterialButton button = new MaterialButton(this, null, R.attr.materialButtonOutlinedStyle);
        liner.addView(button);
        button.setText(text);
        button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#000FFFFF")));
        button.setStrokeColor(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
        button.setTextColor(Color.parseColor("#3F8AE0"));
        button.setAllCaps(false);
        int dp = inPixelFromDp(25);
        button.setCornerRadius(dp);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button.getLayoutParams();
        params.height = inPixelFromDp(41);
        params.weight = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.setMarginEnd(inPixelFromDp(5));
        button.setLayoutParams(params);
        button.setIcon(ContextCompat.getDrawable(this,R.drawable.ic_baseline_close_24));
        button.setIconTintResource(R.color.blueFilter);
        button.setIconSize(inPixelFromDp(16));
        button.setIconGravity(MaterialButton.ICON_GRAVITY_END);
        button.setStrokeColorResource(R.color.blueFilter);

    }



    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/pictures" + "/tmp_screen.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    public void openBottomSheetDialogAddEventsFromunits(View view) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.filter_newevents);
        bottomSheetDialog.show();
    }

    public void showMore(View view) {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        LinearLayout parentLayout = (LinearLayout)view.getParent();
        LinearLayout openLayout = (LinearLayout)parentLayout.getChildAt(2);

        LinearLayout clickLayout = (LinearLayout)view;
        ImageView button = (ImageView)clickLayout.getChildAt(0);

        ViewGroup.LayoutParams params = openLayout.getLayoutParams();

        //openLayout.getVisibility() == View.GONE

        if (openLayout.getHeight() == 0) {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition()); // анимация
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            //openLayout.setVisibility(View.VISIBLE);
            button.setRotation(180);
            clickLayout.setBackgroundColor(Color.parseColor("#F7F5F5"));
        }

        else {
            //openLayout.setVisibility(View.GONE);

            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition());
            params.height = 0;
            button.setRotation(0);
            clickLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        openLayout.setLayoutParams(params);
    }

    // СОЗДАНИЕ НОВОГО ЭЛЕМЕНТА
    public void createElement(JSONObject event) throws JSONException {
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);

        // String dateLimit = event.getString("DateLimit");


        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2021-04-24T13:55:29Z", formatter);*/


        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);

        //--- LinearLayout parentLayout ---
        LinearLayout parentLayout = new LinearLayout(this);
        mainLayout.addView(parentLayout);
        createLinearLayout(parentLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout headerLayout ---
        LinearLayout headerLayout = new LinearLayout(this);
        parentLayout.addView(headerLayout);
        headerLayout.setOnClickListener(this::showMore);

        int dpAsPixels = inPixelFromDp(10);
        headerLayout.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);

        createLinearLayout(headerLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- ImageView arrowImage ---
        ImageView arrowImage = new ImageView(this);
        headerLayout.addView(arrowImage);

        dpAsPixels = inPixelFromDp(20);

        // устанавливаем ширину высоту
        ViewGroup.LayoutParams arrowParams = arrowImage.getLayoutParams();
        arrowParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;;
        arrowParams.width = dpAsPixels;
        arrowImage.setLayoutParams(arrowParams);

        arrowImage.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);


        //--- TextView idText ---
        TextView idText = new TextView(this);
        headerLayout.addView(idText);
        idText.setTypeface(open_sans);

        dpAsPixels = inPixelFromDp(100);

        createTextView(idText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, event.getString("Id"), "#4B4A4D",15);


        //--- LinearLayout nameLayout ---
        LinearLayout nameLayout = new LinearLayout(this);
        headerLayout.addView(nameLayout);
        dpAsPixels = inPixelFromDp(5);
        nameLayout.setPadding(dpAsPixels, 0, dpAsPixels, 0);

        createLinearLayout(nameLayout, 0, ViewGroup.LayoutParams.WRAP_CONTENT, 1, Gravity.CENTER, LinearLayout.HORIZONTAL);


        /*if (event.getString("Criticality").equals("2") || event.getString("Criticality").equals("3")) {
            //--- ImageView fireImage ---
            ImageView fireImage = new ImageView(this);
            nameLayout.addView(fireImage);

            dpAsPixels = inPixelFromDp(20);

            // устанавливаем ширину высоту
            ViewGroup.LayoutParams fireParams = fireImage.getLayoutParams();
            fireParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;;
            fireParams.width = dpAsPixels;
            fireImage.setLayoutParams(fireParams);

            fireImage.setImageResource(R.drawable.ic_baseline_local_fire_department_24);
        }*/


        //--- TextView nameText ---
        TextView nameText = new TextView(this);
        nameLayout.addView(nameText);
        nameText.setTypeface(open_sans);
        nameText.setMaxLines(1);
        nameText.setEllipsize(TextUtils.TruncateAt.END);
        nameText.setSingleLine(true);

        createTextView(nameText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, event.getString("Name"), "#2291EA", 15);


        //--- TextView tegText ---
        TextView leftText = new TextView(this);
        headerLayout.addView(leftText);
        leftText.setTypeface(open_sans);

        dpAsPixels = inPixelFromDp(120);

        // event.getJSONObject("Tag").getString("Name")
        createTextView(leftText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, event.getString("Tag"), "#4B4A4D",15);


        //--- LinearLayout lineLayout ---
        LinearLayout lineLayout = new LinearLayout(this);
        parentLayout.addView(lineLayout);
        lineLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- openLayout openLayout ---
        LinearLayout openLayout = new LinearLayout(this);
        parentLayout.addView(openLayout);
        // openLayout.setVisibility(View.GONE);

        createLinearLayout(openLayout, ViewGroup.LayoutParams.MATCH_PARENT, 0, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout showMoreLayout ---
        LinearLayout showMoreLayout = new LinearLayout(this);
        openLayout.addView(showMoreLayout);

        int dpAsPixelsH = inPixelFromDp(20);
        int dpAsPixelsV = inPixelFromDp(10);
        showMoreLayout.setPadding(dpAsPixelsH, dpAsPixelsV, dpAsPixelsH, dpAsPixelsV);


        createLinearLayout(showMoreLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);

        //event.getJSONObject("Node").getString("Name")
        if (event.has("Node")) {
            createNewItem("Узел", event.getString("Node"), "#2291EA", showMoreLayout);
        }

        // цифры не точные, на сайте нет доступа
        if (event.getString("Status").equals("1")) {
            createNewItem("Статус", "В работе", "#479696", showMoreLayout);
        }

        if (event.getString("Status").equals("3")) {
            createNewItem("Статус", "Выполнен", "#FF5648", showMoreLayout);
        }

        // не знаю как эти поля будут называться в json
        createNewItem("Источник", "тут должен быть источник", "#4B4A4D", showMoreLayout);

        createNewItem("Ответственный", "Пупкин Виктор Александрович", "#2291EA", showMoreLayout);

        createNewItem("Департамент", "Департамент разработки ПО", "#4B4A4D", showMoreLayout);


        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        dpAsPixelsV = inPixelFromDp(10);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER_HORIZONTAL, LinearLayout.VERTICAL);


        /*
        //--- Button button ---
        MaterialButton button = new MaterialButton(this, null, R.attr.materialButtonOutlinedStyle);
        elemLayout.addView(button);

        button.setText("Завершить работу");
        button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#B0F6E4E4")));
        button.setStrokeColor(ColorStateList.valueOf(Color.parseColor("#E78181")));
        dpAsPixels = inPixelFromDp(2);
        button.setStrokeWidth(dpAsPixels);
        button.setTextColor(Color.parseColor("#E6FF5648"));
        dpAsPixels = inPixelFromDp(16);
        button.setCornerRadius(dpAsPixels);
        button.setTextSize(14);
        button.setAllCaps(false);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button.getLayoutParams();
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        button.setLayoutParams(params);
         */

        //--- LinearLayout lineOpenLayout ---
        LinearLayout lineOpenLayout = new LinearLayout(this);
        openLayout.addView(lineOpenLayout);
        lineOpenLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineOpenLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);

    }

    public void createNewItem(String title, String value, String valueColor, LinearLayout showMoreLayout) {
        Typeface raleway = ResourcesCompat.getFont(this, R.font.raleway_semibold_font);
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);
        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        int dpAsPixelsV = inPixelFromDp(10);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.LEFT, LinearLayout.VERTICAL);


        //--- TextView titleText ---
        TextView titleText = new TextView(this);
        elemLayout.addView(titleText);
        titleText.setTypeface(raleway);

        createTextView(titleText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 0, title, "#4B4A4D",15);


        //--- TextView valueText ---
        TextView valueText = new TextView(this);
        elemLayout.addView(valueText);
        valueText.setTypeface(open_sans);
        dpAsPixelsV = inPixelFromDp(6);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) valueText.getLayoutParams();
        params.setMargins(0, dpAsPixelsV, 0, 0);

        createTextView(valueText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 0, value, valueColor,15);

    }

    public int inPixelFromDp(int sizeInDp) {
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp*scale + 0.5f);
        return dpAsPixels;
    }

    public void createTextView(TextView textView, int width, int height, int gravity, int weight, String text, String textColor, int textSize) {
        // устанавливаем ширину высоту и вес
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        textView.setLayoutParams(params);

        textView.setGravity(gravity);
        textView.setText(text);
        textView.setTextColor(Color.parseColor(textColor));
        textView.setTextSize(textSize);
    }

    public void createLinearLayout(LinearLayout linearLayout, int width, int height, int weight, int gravity, int orientation) {
        // устанавливаем ширину высоту и вес

        linearLayout.setOrientation(orientation);
        linearLayout.setGravity(gravity);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        linearLayout.setLayoutParams(params);
    }

}
package com.example.isitoq_project;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.google.android.material.button.MaterialButton;

import org.json.JSONException;
import org.json.JSONObject;

public class infevents_participants extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infevents_participants);

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        JSONObject obj = new JSONObject();

        try {
            obj.put("Id", "108");
            obj.put("User", "Пупкин Виктор Владимирович");
            obj.put("Role", "Наблюдатель");

            createElement(obj);
            createElement(obj);
            createElement(obj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showMore(View view) {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        LinearLayout parentLayout = (LinearLayout)view.getParent();
        LinearLayout openLayout = (LinearLayout)parentLayout.getChildAt(2);

        LinearLayout clickLayout = (LinearLayout)view;
        ImageView button = (ImageView)clickLayout.getChildAt(0);

        ViewGroup.LayoutParams params = openLayout.getLayoutParams();

        if (openLayout.getHeight() == 0) {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition()); // анимация
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;

            button.setRotation(180);
            clickLayout.setBackgroundColor(Color.parseColor("#F7F5F5"));
        }

        else {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition());
            params.height = 0;

            button.setRotation(0);
            clickLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        openLayout.setLayoutParams(params);
    }

    // СОЗДАНИЕ НОВОГО ЭЛЕМЕНТА
    public void createElement(JSONObject user) throws JSONException {
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);

        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);

        //--- LinearLayout parentLayout ---
        LinearLayout parentLayout = new LinearLayout(this);
        mainLayout.addView(parentLayout);
        createLinearLayout(parentLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout headerLayout ---
        LinearLayout headerLayout = new LinearLayout(this);
        parentLayout.addView(headerLayout);
        headerLayout.setOnClickListener(this::showMore);

        int dpAsPixels = inPixelFromDp(10);
        headerLayout.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);

        createLinearLayout(headerLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- ImageView arrowImage ---
        ImageView arrowImage = new ImageView(this);
        headerLayout.addView(arrowImage);

        dpAsPixels = inPixelFromDp(20);

        // устанавливаем ширину высоту
        ViewGroup.LayoutParams arrowParams = arrowImage.getLayoutParams();
        arrowParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;;
        arrowParams.width = dpAsPixels;
        arrowImage.setLayoutParams(arrowParams);

        arrowImage.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);


        //--- TextView idText ---
        TextView idText = new TextView(this);
        headerLayout.addView(idText);
        idText.setTypeface(open_sans);

        dpAsPixels = inPixelFromDp(100);

        createTextView(idText, dpAsPixels, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, user.getString("Id"), "#4B4A4D",15);


        //--- LinearLayout nameLayout ---
        LinearLayout nameLayout = new LinearLayout(this);
        headerLayout.addView(nameLayout);
        dpAsPixels = inPixelFromDp(5);
        nameLayout.setPadding(dpAsPixels, 0, dpAsPixels, 0);

        createLinearLayout(nameLayout, 0, ViewGroup.LayoutParams.WRAP_CONTENT, 1, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- TextView nameText ---
        TextView nameText = new TextView(this);
        nameLayout.addView(nameText);
        nameText.setTypeface(open_sans);
        nameText.setMaxLines(1);
        nameText.setEllipsize(TextUtils.TruncateAt.END);
        nameText.setSingleLine(true);

        createTextView(nameText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL, 0, user.getString("User"), "#2291EA", 15);



        //--- LinearLayout lineLayout ---
        LinearLayout lineLayout = new LinearLayout(this);
        parentLayout.addView(lineLayout);
        lineLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);


        //--- openLayout openLayout ---
        LinearLayout openLayout = new LinearLayout(this);
        parentLayout.addView(openLayout);
        // openLayout.setVisibility(View.GONE);

        createLinearLayout(openLayout, ViewGroup.LayoutParams.MATCH_PARENT, 0, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        //--- LinearLayout showMoreLayout ---
        LinearLayout showMoreLayout = new LinearLayout(this);
        openLayout.addView(showMoreLayout);

        int dpAsPixelsH = inPixelFromDp(20);
        int dpAsPixelsV = inPixelFromDp(10);
        showMoreLayout.setPadding(dpAsPixelsH, dpAsPixelsV, dpAsPixelsH, dpAsPixelsV);


        createLinearLayout(showMoreLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER, LinearLayout.VERTICAL);


        createNewItem("Роль", user.getString("Role"), "#4B4A4D", showMoreLayout);


        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        dpAsPixelsV = inPixelFromDp(10);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.CENTER_HORIZONTAL, LinearLayout.VERTICAL);


        //--- Button button ---
        MaterialButton button = new MaterialButton(this, null, R.attr.materialButtonOutlinedStyle);
        elemLayout.addView(button);

        button.setText("Удалить");
        button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#B0F6E4E4")));
        button.setStrokeColor(ColorStateList.valueOf(Color.parseColor("#E78181")));
        dpAsPixels = inPixelFromDp(2);
        button.setStrokeWidth(dpAsPixels);
        button.setTextColor(Color.parseColor("#E6FF5648"));
        dpAsPixels = inPixelFromDp(16);
        button.setCornerRadius(dpAsPixels);
        button.setTextSize(14);
        button.setAllCaps(false);

        //dpAsPixels = inPixelFromDp(30);
        //button.setPadding(dpAsPixels, 0, dpAsPixels, 0);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button.getLayoutParams();
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        button.setLayoutParams(params);


        //--- LinearLayout lineOpenLayout ---
        LinearLayout lineOpenLayout = new LinearLayout(this);
        openLayout.addView(lineOpenLayout);
        lineOpenLayout.setBackgroundColor(Color.parseColor("#EEECEC"));

        dpAsPixels = inPixelFromDp(1);
        createLinearLayout(lineOpenLayout, ViewGroup.LayoutParams.MATCH_PARENT, dpAsPixels, 0, Gravity.CENTER, LinearLayout.HORIZONTAL);

    }

    public void createNewItem(String title, String value, String valueColor, LinearLayout showMoreLayout) {
        Typeface raleway = ResourcesCompat.getFont(this, R.font.raleway_semibold_font);
        Typeface open_sans = ResourcesCompat.getFont(this, R.font.open_sans_regular);
        //--- LinearLayout elemLayout ---
        LinearLayout elemLayout = new LinearLayout(this);
        showMoreLayout.addView(elemLayout);

        int dpAsPixelsV = inPixelFromDp(10);
        elemLayout.setPadding(0, dpAsPixelsV, 0, dpAsPixelsV);

        createLinearLayout(elemLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, Gravity.LEFT, LinearLayout.VERTICAL);


        //--- TextView titleText ---
        TextView titleText = new TextView(this);
        elemLayout.addView(titleText);
        titleText.setTypeface(raleway);

        createTextView(titleText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 0, title, "#4B4A4D",15);


        //--- TextView valueText ---
        TextView valueText = new TextView(this);
        elemLayout.addView(valueText);
        valueText.setTypeface(open_sans);
        dpAsPixelsV = inPixelFromDp(6);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) valueText.getLayoutParams();
        params.setMargins(0, dpAsPixelsV, 0, 0);

        createTextView(valueText, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.LEFT, 0, value, valueColor,15);
    }

    public int inPixelFromDp(int sizeInDp) {
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp*scale + 0.5f);
        return dpAsPixels;
    }

    public void createTextView(TextView textView, int width, int height, int gravity, int weight, String text, String textColor, int textSize) {
        // устанавливаем ширину высоту и вес
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        textView.setLayoutParams(params);

        textView.setGravity(gravity);
        textView.setText(text);
        textView.setTextColor(Color.parseColor(textColor));
        textView.setTextSize(textSize);
    }

    public void createLinearLayout(LinearLayout linearLayout, int width, int height, int weight, int gravity, int orientation) {
        // устанавливаем ширину высоту и вес

        linearLayout.setOrientation(orientation);
        linearLayout.setGravity(gravity);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params.height = height;
        params.width = width;
        params.weight = weight;
        linearLayout.setLayoutParams(params);
    }
}

package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.isitoq_project.servercontroller.HttpController;

import java.io.IOException;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button loginButton;
    String loginText;
    String passwordText;
    EditText loginInput;
    EditText passwordInput;
    Boolean showText = false;
    HttpController httpController = new HttpController();

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_login_page);
        context = this;

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        loginButton = (Button) findViewById(R.id.button);
        loginButton.setOnClickListener(this);
        loginInput = (EditText) findViewById(R.id.login_input_id);
        passwordInput = (EditText) findViewById(R.id.pass_input_id);


        // отображаем иконки в активном инпуте
        focusInput(passwordInput, "ic_outline_remove_red_eye_24",  "ic_outline_remove_red_eye_24_focus");
        focusInput(loginInput,  "ic_outline_email_24",  "ic_outline_email_24_focus");

        clickIcon(passwordInput);
        clickIcon(loginInput);


    }


//    public static void massegtost(){
//        Toast toast = Toast.makeText(context, "Вы вошли", Toast.LENGTH_LONG);
//        View view1 = toast.getView(); toast.getView().setPadding(20, 20, 20, 20);
//        view1.setBackgroundResource(R.color.white);
//        toast.show();
//    }


    public static void Start(){
        Intent intent = new Intent(context,irp_page.class);
        context.startActivity(intent);
        ((MainActivity)(context)).finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                loginText = loginInput.getText().toString();
                passwordText = passwordInput.getText().toString();
                System.out.println("логин: " + loginText);
                System.out.println("пароль: " + passwordText);
                backgroundWork(loginText, passwordText);
                try {
                    Thread.sleep(700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }
    }


    public void backgroundWork(String stringLogin, String stringPassword) {
        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    httpController.PostLoginHttp(stringLogin, stringPassword);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    @SuppressLint("ClickableViewAccessibility")
    private void clickIcon(EditText currentEditText) {

        currentEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if((event.getAction() == MotionEvent.ACTION_UP) && currentEditText.hasFocus()) {

                    // стираем тест по клику на крестик
                    if(event.getRawX() >= currentEditText.getRight() - currentEditText.getTotalPaddingRight()) {
                        currentEditText.getText().clear();

                        return true;
                    }


                    // показываем/скрываем пароль
                    if(event.getRawX() <= (currentEditText.getCompoundDrawables()[0].getBounds().width() + currentEditText.getTotalPaddingLeft()))  {
                        if (passwordInput.hasFocus()) {
                            if (!showText) {
                                currentEditText.setTransformationMethod(null);
                                showText = true;
                            }
                            else {
                                currentEditText.setTransformationMethod(new PasswordTransformationMethod());
                                showText = false;
                            }
                            return true;
                        }
                    }

                }
                return false;
            }
        });
    }


    @SuppressLint("ClickableViewAccessibility")
    private void focusInput(EditText currentEditText, String normalIcon, String activeIcon) {
        currentEditText.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    currentEditText.setCompoundDrawablesWithIntrinsicBounds(getResources().getIdentifier(activeIcon, "drawable", getPackageName()), 0, R.drawable.ic_baseline_close_24, 0);
                }

                else {
                    currentEditText.setCompoundDrawablesWithIntrinsicBounds(getResources().getIdentifier(normalIcon, "drawable", getPackageName()), 0, 0, 0);
                }
            }
        });
    }
}






/*
class GradientTextView extends TextView {

    public GradientTextView(Context context) {
        super(context, null, -1);
    }

    public GradientTextView(Context context, AttributeSet attrs) {
        super(context, attrs, -1);
    }

    public GradientTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
                            int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            getPaint().setShader(
                    new LinearGradient(0, 0, 0, getHeight(), Color.YELLOW,
                            Color.BLACK, Shader.TileMode.CLAMP));
        }
    }
} */
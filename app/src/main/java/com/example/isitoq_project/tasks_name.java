package com.example.isitoq_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class tasks_name extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tasks_name);
    }


    public void showMore(View view) {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        CardView clickLayout = (CardView) view;
        LinearLayout parentLayout = (LinearLayout)clickLayout.getChildAt(0);
        LinearLayout openLayout = (LinearLayout)parentLayout.getChildAt(1);

        LinearLayout headerLayout = (LinearLayout)parentLayout.getChildAt(0);
        LinearLayout innerLayout = (LinearLayout)headerLayout.getChildAt(0);
        ImageButton button = (ImageButton)innerLayout.getChildAt(1);

        ViewGroup.LayoutParams params = openLayout.getLayoutParams();

        if (openLayout.getHeight() == 0) {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition()); // анимация
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            button.setRotation(90);
            button.setImageTintList(ColorStateList.valueOf(Color.parseColor("#B8C1CC")));
        }

        else {
            TransitionManager.beginDelayedTransition(mainLayout, new AutoTransition());
            params.height = 0;
            button.setRotation(0);
            button.setImageTintList(ColorStateList.valueOf(Color.parseColor("#3F8AE0")));
        }

        openLayout.setLayoutParams(params);
    }
}